# Distributed Press API client

This is an API client for [Distributed Press](https://distributed.press/).

Supports basic configuration and uploading the website's directory.  It
will create a tarball on the fly, and uses `IO` to minimize memory
usage.  May not work on every operating system.

## Installation

Add this line to your Gemfile:

```ruby
gem 'distributed-press-api-client'
```

And then execute:

```bash
bundle
```

Or install it yourself as:

```bash
gem install distributed-press-api-client
```

## Usage

```ruby
require 'distributed_press'

dp = DistributedPress.new
       url: 'https://api.distributed.press',
       api_key: 'https://github.com/hyphacoop/api.distributed.press#steps',
       project_domain: 'example.org'

dp.configure
dp.publish '/path/to/your/site'
```

API key and project domain can be passed as environment variables called
`DISTRIBUTED_PRESS_API_KEY` and `DISTRIBUTED_PRESS_PROJECT_DOMAIN`
respectively.  API URL is optional and defaults to
`https://api.distributed.press`.

## Jekyll plugin

**Note:** May be moved to its own repository.

Install and add to `_config.yml`:

```yaml
plugins:
- jekyll-distributed-press
```

Build and publish site:

```bash
JEKYLL_ENV=production DISTRIBUTED_PRESS_API_KEY=the_api_key bundle exec jekyll build
```

Other env vars:

```bash
DISTRIBUTED_PRESS_PROJECT_DOMAIN=your.domain.at.distributed.press
DISTRIBUTED_PRESS_API_URL=https://api.distributed.press
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/distributed-press-api-client>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the Affero GPL3
License.

## Code of Conduct

Everyone interacting in the distributed-press-api-client project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).
