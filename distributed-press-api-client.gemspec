# frozen_string_literal: true

require_relative 'lib/distributed_press/version'

Gem::Specification.new do |spec|
  spec.name          = 'distributed-press-api-client'
  spec.version       = DistributedPress::VERSION
  spec.authors       = %w[f]
  spec.email         = %w[f@sutty.nl]

  spec.summary       = 'Distributed Press API Client'
  spec.description   = 'An API client for Distributed Press (https://distributed.press/)'
  spec.homepage      = "https://0xacab.org/sutty/#{spec.name}"
  spec.license       = 'AGPL-3.0'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.7')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.cert_chain = [File.expand_path('~/gem-public_cert.pem')]
  spec.signing_key = File.expand_path('~/gem-private_key.pem')

  spec.files         = Dir['lib/**/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_runtime_dependency 'addressable', '~> 2.3', '>= 2.3.0'
  spec.add_runtime_dependency 'climate_control'
  spec.add_runtime_dependency 'dry-schema'
  spec.add_runtime_dependency 'httparty', '~> 0.18'
  spec.add_runtime_dependency 'httparty-cache', '~> 0.0.6'
  spec.add_runtime_dependency 'json', '~> 2.1', '>= 2.1.0'
  spec.add_runtime_dependency 'jwt', '~> 2.6.0'
  spec.add_runtime_dependency 'nokogiri', '~> 1.16'

  spec.add_development_dependency 'activesupport'
  spec.add_development_dependency 'factory_bot'
  spec.add_development_dependency 'nanoid'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'
  spec.add_development_dependency 'rspec-tap-formatters'
  spec.add_development_dependency 'yard'

  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'ruby-brs'
  spec.add_development_dependency 'webmock', '~> 1.24', '>= 1.24.3'
end
