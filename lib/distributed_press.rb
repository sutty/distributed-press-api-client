# frozen_string_literal: true

require 'httparty'
require 'open3'
require_relative 'distributed_press/multipart'

# Distributed Press (https://distributed.press) API client
class DistributedPress
  include ::HTTParty

  # Default URL
  DEFAULT_URL = 'https://api.distributed.press'

  attr_reader :api_key, :project_domain, :configure_result, :publish_result

  # @param url [String] API URL
  # @param api_key [String] API Key
  # @param project_domain [String] Domain name
  def initialize(url: DEFAULT_URL, api_key: nil, project_domain: nil)
    url ||= DEFAULT_URL
    @api_key = api_key || ENV['DISTRIBUTED_PRESS_API_KEY']
    @project_domain = project_domain || ENV['DISTRIBUTED_PRESS_PROJECT_DOMAIN']

    raise ArgumentError, 'API key is missing' unless @api_key
    raise ArgumentError, 'Project domain is missing' unless @project_domain

    self.class.default_options[:base_uri] = HTTParty.normalize_base_uri(url)
  end

  # Configure the domain
  #
  # @return [Boolean] Configuration was successful
  def configure
    stream, config = IO.pipe
    multipart = Multipart.new

    Thread.new do
      multipart.write_header config, 'config.json'
      IO.copy_stream StringIO.new({ 'domain' => project_domain }.to_json), config
      multipart.write_footer config

      config.close
    end

    @configure_result = upload_io('/v0/publication/configure', stream, multipart.boundary)

    configure_result['errorCode'].zero?
  end

  # Publishes a directory by tarballing it on the fly.
  #
  # @param path [String] Directory path
  # @return [Boolean] Upload was successful
  def publish(path)
    raise ArgumentError, "#{path} is not a directory" unless File.directory? path

    filename = File.basename path

    Open3.popen2('tar', '--to-stdout', '--create', '--gzip', '--dereference', '--directory', path,
                 '.') do |_stdin, stdout, thread|
      stream, body = IO.pipe

      multipart = Multipart.new

      # Generate a multipart body and copy contents to it.
      Thread.new do
        multipart.write_header body, "#{filename}.tar.gz"
        IO.copy_stream stdout, body
        multipart.write_footer body

        body.close
      end

      @publish_result = upload_io '/v0/publication/publish', stream, multipart.boundary

      # wait
      thread.value
      publish_result['errorCode'].zero?
    end
  end

  private

  # Upload an IO object
  #
  # @param endpoint [String]
  # @param io [IO]
  # @param boundary [String]
  def upload_io(endpoint, io, boundary)
    self.class.post(endpoint, body_stream: io, headers: headers(
      'Accept' => 'application/json',
      'Content-Type' => "multipart/form-data; boundary=#{boundary}",
      'Transfer-Encoding' => 'chunked'
    ))
  end

  # Default request headers
  #
  # @param extra [Hash] Extra headers
  # @return [Hash]
  def headers(extra = {})
    extra.merge('Accept' => 'application/json', 'Authorization' => "Bearer #{api_key}")
  end
end
