# frozen_string_literal: true

require 'httparty'

class DistributedPress
  # Deals with multipart requests
  class Multipart
    # Newlines
    NEWLINE  = "\r\n"

    # Boundary separator
    BOUNDARY = '--'

    # Write the multipart header
    #
    # @param io [IO]
    # @param filename [String]
    def write_header(io, filename)
      io.write BOUNDARY
      io.write boundary
      io.write NEWLINE
      io.write "Content-Disposition: form-data; name=\"file\"; filename=\"#{filename}\""
      io.write NEWLINE
      io.write 'Content-Type: application/octet-stream'
      io.write NEWLINE
      io.write NEWLINE

      nil
    end

    # Write the multipart footer
    #
    # @param io [IO]
    def write_footer(io)
      io.write NEWLINE
      io.write BOUNDARY
      io.write boundary
      io.write BOUNDARY
      io.write NEWLINE

      nil
    end

    # Generate a MultiPart boundary and reuse it
    #
    # @return [String]
    def boundary
      @boundary ||= HTTParty::Request::MultipartBoundary.generate
    end
  end
end
