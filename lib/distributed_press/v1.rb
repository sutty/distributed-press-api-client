# frozen_string_literal: true

require_relative 'v1/client'
require_relative 'v1/client/admin'
require_relative 'v1/client/auth'
require_relative 'v1/client/publisher'
require_relative 'v1/client/site'
require_relative 'v1/schemas'

# API Client
class DistributedPress
  # APIv1
  module V1
  end
end
