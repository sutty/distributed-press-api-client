# frozen_string_literal: true

require 'httparty'
require 'open3'

require_relative '../version'
require_relative 'token'
require_relative 'errors'
require_relative 'schemas/site'
require_relative 'schemas/new_site'
require_relative 'schemas/update_site'

class DistributedPress
  module V1
    # Distributed Press APIv1 client
    class Client
      include ::HTTParty

      # API URL
      # @return [String]
      attr_reader :url

      # Auth token
      # @return [DistributedPress::V1:Token]
      attr_reader :token

      # Initializes a client with a API address and token.  Depending on
      # the token capabilities we'll be able to do some actions or not.
      #
      # Automatically gets a new token if it's about to expire
      #
      # @param url [String]
      # @param token [String,Distributed::Press::V1::Token]
      # @param logger [Logger]
      def initialize(url: 'https://api.distributed.press', token: nil, logger: nil)
        self.class.default_options[:base_uri] = @url = HTTParty.normalize_base_uri(url)
        self.class.default_options[:logger] = logger if logger
        self.class.default_options[:log_format] = :logstash

        @token =
          case token
          when Token then token
          else Token.new(token: token.to_s)
          end

        return if @token.forever?

        raise TokenExpiredError if @token.expired?

        exchange_token! if @token.expires_in_x_seconds < 60
      end

      # GET request
      #
      # @param endpoint [String]
      # @return [Hash]
      def get(endpoint:)
        self.class.get(endpoint, headers: headers).tap do |response|
          process_response_errors! endpoint, response
        end
      end

      # POST request
      #
      # @param endpoint [String]
      # @param schema [Dry::Schema::Result]
      # @return [Hash]
      def post(endpoint:, schema:)
        self.class.post(endpoint, body: schema.to_h.to_json, headers: headers).tap do |response|
          process_response_errors! endpoint, response
        end
      end

      # PUT request
      #
      # @param endpoint [String]
      # @param io [IO]
      # @param boundary [String]
      # @param timeout [Integer]
      # @return [Hash]
      def put(endpoint:, io:, boundary:, timeout: 600)
        multipart_headers = headers.dup
        multipart_headers['Content-Type'] = "multipart/form-data; boundary=#{boundary}"
        multipart_headers['Transfer-Encoding'] = 'chunked'

        self.class.put(endpoint, body_stream: io, headers: multipart_headers, max_retries: 0,
                                 timeout: timeout).tap do |response|
          process_response_errors! endpoint, response
        end
      end

      # PATCH request
      #
      # @param endpoint [String]
      # @param schema [Dry::Schema::Result]
      def patch(endpoint:, schema:); end

      # DELETE request, they return 200 when deletion is confirmed.
      #
      # @param endpoint [String]
      # @param schema [Dry::Schema::Result]
      # @return [Boolean]
      def delete(endpoint:)
        self.class.delete(endpoint, headers: headers).tap do |response|
          process_response_errors! endpoint, response
        end.ok?
      end

      # Exchanges a token for a new one if expired.  Modifies the token
      # used for initialization.  If you need the token for subsequent
      # requests you can chain them.
      def exchange_token!
        raise TokenCapabilityMissingError, 'Exchanging tokens requires refresh capability' unless token.refresh?

        new_token_payload = Schemas::NewTokenPayload.new.call(capabilities: token.capabilities)

        @token = Token.new(token: post(endpoint: '/v1/auth/exchange', schema: new_token_payload).body)
        nil
      end

      # Default headers
      #
      # @return [Hash]
      def headers
        @headers ||= {
          'User-Agent' => "DistributedPress/#{DistributedPress::VERSION}",
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Authorization' => "Bearer #{token}"
        }
      end

      private

      # Raise errors on server responses
      #
      # @param endpoint [String]
      # @param response [HTTParty::Response]
      def process_response_errors!(endpoint, response)
        raise TokenUnauthorizedError if response.unauthorized?
        raise Error, "#{endpoint}: #{response.code} #{response.to_s.tr("\n", '')}" unless response.ok?
      end
    end
  end
end
