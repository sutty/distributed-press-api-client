# frozen_string_literal: true

require_relative '../client'

class DistributedPress
  module V1
    class Client
      # Admins can create and remove publishers
      class Admin
        attr_reader :client

        # @param client [DistributedPress::V1::Client]
        def initialize(client)
          @client = client
        end

        # Creates a Admin, requires a token with admin capabilities
        #
        # @param schema [DistributedPress::V1::Schemas::NewAdmin]
        # @return [DistributedPress::V1::Schemas::Admin]
        def create(schema)
          validate_schema! schema
          validate_capabilities!

          Schemas::Admin.new.call(**client.post(endpoint: '/v1/admin', schema: schema))
        end

        # Deletes a Admin, requires a token with admin capabilities
        #
        # @param schema [DistributedPress::V1::Schemas::Admin]
        # @return [Boolean]
        def delete(schema)
          validate_schema! schema
          validate_capabilities!

          client.delete(endpoint: "/v1/admin/#{schema[:id]}")
        end

        private

        # Raises an error if the schema is not valid
        def validate_schema!(schema)
          raise SchemaNotValidError unless schema.success?
        end

        # Raises an error if the client doesn't have the capabilities
        def validate_capabilities!
          raise TokenCapabilityMissingError, 'Token needs admin capability' unless client.token.admin?
        end
      end
    end
  end
end
