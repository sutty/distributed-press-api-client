# frozen_string_literal: true

require_relative '../client'
require_relative '../token'

class DistributedPress
  module V1
    class Client
      # Manage auth
      class Auth
        attr_reader :client

        # @param client [DistributedPress::V1::Client]
        def initialize(client)
          @client = client
        end

        # Exchanges a token for another one
        #
        # @param schema [DistributedPress::V1::Schemas::NewTokenPayload]
        # @return [DistributedPress::V1::Token]
        def exchange(schema)
          validate_schema! schema
          validate_capabilities! schema

          Token.new(token: client.post(endpoint: '/v1/auth/exchange', schema: schema).body)
        end

        # Revokes a token
        #
        # @param schema [DistributedPress::V1::Schemas::TokenPayload]
        # @return [Boolean]
        def revoke(schema)
          validate_schema! schema

          raise TokenCapabilityMissingError, 'Only admins can revoke tokens' unless client.token.admin?

          client.delete(endpoint: "/v1/auth/revoke/#{schema[:tokenId]}")
        end

        private

        # Raises an error if the schema is not valid
        def validate_schema!(schema)
          raise SchemaNotValidError unless schema.success?
        end

        # Raises an error if the client doesn't have the capabilities
        # required.  Admins can do everything.
        def validate_capabilities!(schema)
          return if client.token.admin?

          %w[publisher refresh].each do |capability|
            if schema[:capabilities].include?(capability) && !client.token.public_send(:"#{capability}?")
              raise TokenCapabilityMissingError, "Token needs #{capability} capability"
            end
          end
        end
      end
    end
  end
end
