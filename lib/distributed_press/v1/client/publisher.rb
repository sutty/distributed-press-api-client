# frozen_string_literal: true

require_relative '../client'

class DistributedPress
  module V1
    class Client
      # Publishers can create and update sites
      class Publisher
        # Client instance
        #
        # @return [DistributedPress::V1::Client]
        attr_reader :client

        # @param client [DistributedPress::V1::Client]
        def initialize(client)
          @client = client
        end

        # Creates a Publisher, requires a token with admin capabilities
        #
        # @param schema [DistributedPress::V1::Schemas::NewPublisher]
        # @return [DistributedPress::V1::Schemas::Publisher]
        def create(schema)
          validate_schema! schema
          validate_capabilities!

          Schemas::Publisher.new.call(**client.post(endpoint: '/v1/publisher', schema: schema))
        end

        # Deletes a Publisher, requires a token with admin capabilities
        #
        # @param schema [DistributedPress::V1::Schemas::Publisher]
        # @return [Boolean]
        def delete(schema)
          validate_schema! schema
          validate_capabilities!

          client.delete(endpoint: "/v1/publisher/#{schema[:id]}")
        end

        private

        # Raises an error if the schema is not valid
        def validate_schema!(schema)
          raise SchemaNotValidError unless schema.success?
        end

        # Raises an error if the client doesn't have the capabilities
        def validate_capabilities!
          raise TokenCapabilityMissingError, 'Token needs admin capability' unless client.token.admin?
        end
      end
    end
  end
end
