# frozen_string_literal: true

require 'open3'

require_relative '../client'
require_relative '../errors'
require_relative '../../multipart'

class DistributedPress
  module V1
    class Client
      # Sites can be created and updated to set distribution options,
      # and a tarball with their contents can be sent to Distributed
      # Press.
      class Site
        # Client instance
        #
        # @return [DistributedPress::V1::Client]
        attr_reader :client

        # @param client [DistributedPress::V1::Client]
        def initialize(client)
          @client = client
        end

        # Retrieves information about a site
        #
        # @param schema [DistributedPress::V1::Schemas::PublishingSite]
        def show(schema)
          validate_schema! schema
          validate_capabilities!

          Schemas::Site.new.call(**client.get(endpoint: "/v1/sites/#{schema[:id]}"))
        end

        # Creates a website
        #
        # @param schema [DistributedPress::V1::Schemas::NewSite]
        # @return [DistributedPress::V1::Schemas::Site]
        def create(schema)
          validate_schema! schema
          validate_capabilities!

          Schemas::Site.new.call(**client.post(endpoint: '/v1/sites', schema: schema))
        end

        # Updates a website configuration
        #
        # @param schema [DistributedPress::V1::Schemas::UpdateSite]
        # @return [DistributedPress::V1::Schemas::Site]
        def update(schema)
          validate_schema! schema
          validate_capabilities!

          Schemas::Site.new.call(**client.post(endpoint: "/v1/sites/#{schema[:id]}", schema: schema))
        end

        # Publish changes to a website
        #
        # @param schema [DistributedPress::V1::Schemas::PublishingSite]
        # @param path [String,Pathname]
        # @return [Bool]
        def publish(schema, path)
          validate_schema! schema
          validate_capabilities!
          responses = []

          Open3.popen2('tar', '--to-stdout', '--create', '--gzip',
                       '--dereference', '--exclude="*.gz"',
                       '--exclude="*.br"', '--directory', path,
                       '.') do |_, stdout, thread|
            stream, body = IO.pipe
            multipart = Multipart.new

            Thread.new do
              multipart.write_header body, 'site.tar.gz'
              IO.copy_stream stdout, body
              multipart.write_footer body

              body.close
            end

            responses << client.put(endpoint: "/v1/sites/#{schema[:id]}", io: stream, boundary: multipart.boundary)

            # wait for tar to finish
            responses << thread.value
          end

          responses.all?(&:success?)
        end

        # Deletes a website
        #
        # @param schema [DistributedPress::V1::Schemas::Site]
        # @return [Boolean]
        def delete(schema)
          validate_schema! schema
          validate_capabilities!

          client.delete(endpoint: "/v1/sites/#{schema[:id]}")
        end

        private

        # Raises an error if the schema is not valid
        def validate_schema!(schema)
          raise SchemaNotValidError unless schema.success?
        end

        # Raises an error if the client doesn't have the capabilities
        def validate_capabilities!
          return if client.token.publisher? || client.token.admin?

          raise TokenCapabilityMissingError, 'Token needs publisher or admin capability'
        end
      end
    end
  end
end
