# frozen_string_literal: true

class DistributedPress
  module V1
    # Base error
    class Error < StandardError; end

    # Token errors
    class TokenError < Error; end

    # Schema errors
    class SchemaError < Error; end

    # Header not valid
    class TokenHeaderNotValidError < TokenError
      def initialize(msg = 'Token header not valid')
        super
      end
    end

    # Payload not valid
    class TokenPayloadNotValidError < TokenError
      def initialize(msg = 'Token payload not valid')
        super
      end
    end

    # A token expired can't be exchanged for a new one
    class TokenExpiredError < TokenError
      def initialize(msg = 'Token expired! Get a new token from an admin')
        super
      end
    end

    # A capability is missing
    class TokenCapabilityMissingError < TokenError; end

    # Token is not authorized to do this action or is revoked
    class TokenUnauthorizedError < TokenError
      def initialize(msg = 'Token is not authorized or revoked')
        super
      end
    end

    # Schema doesn't have the required information
    class SchemaNotValidError < SchemaError
      def initialize(msg = 'Schema is not valid')
        super
      end
    end
  end
end
