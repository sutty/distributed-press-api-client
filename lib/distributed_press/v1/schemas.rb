# frozen_string_literal: true

require_relative 'schemas/admin'
require_relative 'schemas/http_protocol'
require_relative 'schemas/hyper_protocol'
require_relative 'schemas/ipfs_protocol'
require_relative 'schemas/bittorrent_protocol'
require_relative 'schemas/new_admin'
require_relative 'schemas/new_publisher'
require_relative 'schemas/new_site'
require_relative 'schemas/new_token_payload'
require_relative 'schemas/publisher'
require_relative 'schemas/publishing_site'
require_relative 'schemas/site'
require_relative 'schemas/token_header'
require_relative 'schemas/token_payload'
require_relative 'schemas/update_site'

class DistributedPress
  module V1
    # Schema validation
    module Schemas
    end
  end
end
