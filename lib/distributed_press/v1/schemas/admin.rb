# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Schema for full admin
      class Admin < Dry::Schema::JSON
        define do
          required(:id).filled(:string)
          required(:name).filled(:string)
        end
      end
    end
  end
end
