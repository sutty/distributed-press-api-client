# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Bittorrent protocol
      class BittorrentProtocol < Dry::Schema::JSON
        define do
          required(:enabled).value(:bool)
          # TODO: Validate URL
          required(:link).filled(:string)
          required(:gateway).filled(:string)
          required(:dnslink).filled(:string)
          required(:infoHash).filled(:string)
          required(:pubKey).filled(:string)
          required(:magnet).filled(:string)
        end
      end
    end
  end
end
