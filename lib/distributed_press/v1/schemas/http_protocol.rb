# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # HTTP protocol
      class HttpProtocol < Dry::Schema::JSON
        define do
          required(:enabled).value(:bool)
          # TODO: Validate URL
          required(:link).filled(:string)
        end
      end
    end
  end
end
