# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Hypercore protocol
      class HyperProtocol < Dry::Schema::JSON
        define do
          required(:enabled).value(:bool)
          # TODO: Validate URL
          required(:link).filled(:string)
          required(:gateway).filled(:string)
          required(:raw).filled(:string)
        end
      end
    end
  end
end
