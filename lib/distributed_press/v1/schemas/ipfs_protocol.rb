# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # IPFS protocol
      class IpfsProtocol < Dry::Schema::JSON
        define do
          required(:enabled).value(:bool)
          # TODO: Validate URL
          required(:link).filled(:string)
          required(:gateway).filled(:string)
          required(:cid).filled(:string)
          required(:pubKey).filled(:string)
        end
      end
    end
  end
end
