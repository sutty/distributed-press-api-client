# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Schema representing a new admin.
      class NewAdmin < Dry::Schema::JSON
        define do
          required(:name).filled(:string)
        end
      end
    end
  end
end
