# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Schema representing a new site.  They are created by domain and
      # optionally protocols enabled.
      class NewSite < Dry::Schema::JSON
        define do
          # TODO: Validate domain name
          required(:domain).filled(:string)
          optional(:public).filled(:bool)

          optional(:protocols).hash do
            optional(:http).filled(:bool)
            optional(:ipfs).filled(:bool)
            optional(:hyper).filled(:bool)
          end
        end
      end
    end
  end
end
