# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # JWT payload for new tokens
      class NewTokenPayload < Dry::Schema::JSON
        # Capabilities supported by the APIv1
        #
        # @return [Array]
        CAPABILITIES = %w[publisher admin refresh].freeze

        define do
          optional(:issuedTo).filled(:string)
          required(:capabilities).filled(:array).each do
            included_in? CAPABILITIES
          end
        end
      end
    end
  end
end
