# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Schema for full publisher
      class Publisher < Dry::Schema::JSON
        define do
          required(:id).filled(:string)
          required(:name).filled(:string)
          required(:ownedSites).array(:string)
        end
      end
    end
  end
end
