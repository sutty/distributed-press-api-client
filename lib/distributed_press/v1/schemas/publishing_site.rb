# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Schema for publishing a site
      class PublishingSite < Dry::Schema::JSON
        define do
          required(:id).filled(:string)
        end
      end
    end
  end
end
