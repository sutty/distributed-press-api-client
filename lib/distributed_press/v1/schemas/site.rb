# frozen_string_literal: true

require 'dry-schema'
require_relative 'http_protocol'
require_relative 'hyper_protocol'
require_relative 'ipfs_protocol'
require_relative 'bittorrent_protocol'

class DistributedPress
  module V1
    module Schemas
      # Schema for full site, usually the APIv1 responds with this.
      class Site < Dry::Schema::JSON
        define do
          required(:id).filled(:string)

          # TODO: Validate domain name
          required(:domain).filled(:string)
          optional(:public).filled(:bool)

          required(:protocols).hash do
            required(:http).filled(:bool)
            required(:ipfs).filled(:bool)
            required(:hyper).filled(:bool)
            optional(:bittorrent).filled(:bool)
          end

          required(:links).hash do
            optional(:http).hash(HttpProtocol.new)
            optional(:hyper).hash(HyperProtocol.new)
            optional(:ipfs).hash(IpfsProtocol.new)
            optional(:bittorrent).hash(BittorrentProtocol.new)
          end
        end
      end
    end
  end
end
