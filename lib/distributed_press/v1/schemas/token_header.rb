# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # JWT header
      class TokenHeader < Dry::Schema::JSON
        define do
          required(:alg).filled(:string)
          optional(:typ).filled(:string)
        end
      end
    end
  end
end
