# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # JWT payload
      class TokenPayload < Dry::Schema::JSON
        # Capabilities supported by the APIv1
        #
        # @return [Array]
        CAPABILITIES = %w[publisher admin refresh].freeze

        define do
          required(:issuedTo).filled(:string)
          required(:tokenId).filled(:string)
          required(:iat).filled(:integer, gt?: 0)
          required(:expires).filled(:integer, gt?: -2)
          required(:capabilities).filled(:array).each do
            included_in? CAPABILITIES
          end
        end
      end
    end
  end
end
