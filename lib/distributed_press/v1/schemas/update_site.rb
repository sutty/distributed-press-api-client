# frozen_string_literal: true

require 'dry-schema'

class DistributedPress
  module V1
    module Schemas
      # Schema for updating a website options.  ID is required to
      # generate URL.
      class UpdateSite < Dry::Schema::JSON
        define do
          required(:id).filled(:string)
          optional(:public).filled(:bool)

          required(:protocols).hash do
            optional(:http).filled(:bool)
            optional(:ipfs).filled(:bool)
            optional(:hyper).filled(:bool)
          end
        end
      end
    end
  end
end
