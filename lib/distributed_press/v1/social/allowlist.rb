# frozen_string_literal: true

require_relative 'blocklist'

class DistributedPress
  module V1
    module Social
      # Manages Social Inbox allowlist
      #
      # @example
      #   DistributedPress::V1::Social::Allowlist.new(client: client)
      #   DistributedPress::V1::Social::Allowlist.new(client: client, actor: '@sutty@sutty.nl')
      class Allowlist < Blocklist
        ENDPOINT = '/allowlist'
      end
    end
  end
end
