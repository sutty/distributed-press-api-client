# frozen_string_literal: true

require_relative 'client'

class DistributedPress
  module V1
    module Social
      # Manages Social Inbox blocklist
      #
      # @example
      #   DistributedPress::V1::Social::Blocklist.new(client: client)
      #   DistributedPress::V1::Social::Blocklist.new(client: client, actor: '@sutty@sutty.nl')
      class Blocklist
        ACCEPT = %w[text/plain].freeze
        CONTENT_TYPE = 'text/plain'
        ENDPOINT = '/blocklist'

        # @return [DistributedPress::V1::Social::Client]
        attr_reader :client

        # @return [String,nil]
        attr_reader :actor

        # @param :client [DistributedPress::V1::Social::Client]
        # @param :actor [String]
        def initialize(client:, actor: nil)
          @client = client
          @actor = actor

          @serializer = proc do |body|
            body.join("\n")
          end

          # @param :body [String,nil]
          # @return [Array<String>]
          @parser =
            proc do |body, _|
              next [] if body.nil?

              body.split("\n").map(&:strip).reject(&:empty?)
            end
        end

        # Obtains the current blocklist
        #
        # @return [Array<String>]
        def get
          client.get(endpoint: endpoint, parser: @parser, content_type: CONTENT_TYPE, accept: ACCEPT)
        end

        # Sends an array of instances and accounts to be blocked
        #
        # @param :list [Array<String>]
        # @return [HTTParty::Response]
        def post(list:)
          client.post(endpoint: endpoint, body: list, serializer: @serializer, parser: @parser,
                      content_type: CONTENT_TYPE, accept: ACCEPT)
        end

        # Removes instances and accounts from the blocklist
        #
        # @param :list [Array<String>]
        # @return [HTTParty::Response]
        def delete(list:)
          client.delete(endpoint: endpoint, body: list, serializer: @serializer, parser: @parser,
                        content_type: CONTENT_TYPE, accept: ACCEPT)
        end

        def endpoint
          @endpoint ||= "/v1/#{actor}#{self.class::ENDPOINT}".squeeze('/')
        end
      end
    end
  end
end
