# frozen_string_literal: true

require 'httparty'
require 'httparty/cache'
require 'openssl'
require 'time'
require 'digest/sha2'
require 'uri'
require_relative '../../version'
require_relative '../../../httparty/parser/nokogiri'
require_relative 'signed_headers'

# Retro-compatibility for URI < 0.12
unless URI.respond_to?(:encode_uri_component)
  URI.class_eval do
    def self.encode_uri_component(str, enc = nil)
      encode_www_form_component(str, enc || Encoding::UTF_8).gsub('+', '%20')
    end
  end
end

class DistributedPress
  module V1
    module Social
      # Social Distributed Press APIv1 client
      #
      # Inspired by Mastodon's Request
      #
      # @todo It'd be nice to implement this on HTTParty itself
      # @see {https://github.com/mastodon/mastodon/blob/main/app/lib/request.rb}
      class Client
        include ::HTTParty
        include ::HTTParty::Cache

        class Error < StandardError; end
        class ContentTooLargeError < Error; end
        class BrotliUnsupportedError < Error; end
        class ActivityIdTooLargeError < Error; end
        class EmptyResponseError < Error; end

        # Always cache
        caching true

        # Content types sent and accepted
        ACCEPT = %w[application/activity+json application/ld+json application/json].freeze
        CONTENT_TYPE = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'

        ACCEPT.each do |accept|
          HTTParty::Parser::SupportedFormats[accept] = :json
        end

        # API URL
        #
        # @return [String]
        attr_reader :url

        # API URI
        #
        # @return [URI]
        attr_reader :uri

        # RSA key size
        #
        # @return [Integer]
        attr_reader :key_size

        # Public key URL
        #
        # @return [String]
        attr_reader :public_key_url

        # Logger
        #
        # @return [Logger]
        attr_reader :logger

        # @param :url [String] Social Distributed Press URL
        # @param :public_key [String] URL where public key is available
        # @param :private_key_pem [String] RSA Private key, PEM encoded
        # @param :key_size [Integer]
        # @param :logger [Logger]
        # @param :cache_store [HTTParty::Cache::Store::Abstract,Symbol]
        def initialize(public_key_url:, url: 'https://social.distributed.press', private_key_pem: nil, key_size: 2048,
                       logger: nil, cache_store: :memory)
          self.class.default_options[:logger] = @logger = logger
          self.class.default_options[:log_level] = :debug
          self.class.cache_store cache_store

          @url = HTTParty.normalize_base_uri(url)
          @public_key_url = public_key_url
          @key_size = key_size
          @private_key_pem = private_key_pem
          @uri = URI.parse(url)
          @serializer ||= proc do |body|
            body.to_json
          end
        end

        def _dump(_)
          Marshal.dump({
                         public_key_url: public_key_url,
                         url: url,
                         private_key_pem: @private_key_pem,
                         key_size: key_size,
                         cache_store: self.class.cache_store
                       })
        end

        def self._load(hash)
          new(**Marshal.load(hash))
        end

        # GET request
        #
        # @param endpoint [String]
        # @param parser [HTTParty::Parser,Proc]
        # @param content_type [String]
        # @param accept [Array<String>]
        # @return [HTTParty::Response]
        def get(endpoint:, parser: HTTParty::Parser, content_type: CONTENT_TYPE, accept: ACCEPT)
          perform_signed_request method: Net::HTTP::Get, endpoint: endpoint, parser: parser, content_type: content_type,
                                 accept: accept
        end

        # POST request
        #
        # @todo Use DRY-schemas
        # @param endpoint [String]
        # @param body [Hash]
        # @param serializer [Proc]
        # @param parser [HTTParty::Parser,Proc]
        # @param content_type [String]
        # @param accept [Array<String>]
        # @return [HTTParty::Response]
        def post(endpoint:, body: nil, serializer: @serializer, parser: HTTParty::Parser, content_type: CONTENT_TYPE,
                 accept: ACCEPT)
          perform_signed_request method: Net::HTTP::Post, endpoint: endpoint, body: body, serializer: serializer,
                                 parser: parser, content_type: content_type, accept: accept
        end

        # PUT request
        #
        # @param endpoint [String]
        # @param body [Hash]
        # @param serializer [Proc]
        # @param parser [HTTParty::Parser,Proc]
        # @param content_type [String]
        # @param accept [Array<String>]
        # @return [HTTParty::Response]
        def put(endpoint:, body: nil, serializer: @serializer, parser: HTTParty::Parser, content_type: CONTENT_TYPE,
                accept: ACCEPT)
          perform_signed_request method: Net::HTTP::Put, endpoint: endpoint, body: body, serializer: serializer,
                                 parser: parser, content_type: content_type, accept: accept
        end

        # DELETE request with optional body
        #
        # @param endpoint [String]
        # @param body [Hash]
        # @param serializer [Proc]
        # @param parser [HTTParty::Parser,Proc]
        # @param content_type [String]
        # @param accept [Array<String>]
        # @return [HTTParty::Response]
        def delete(endpoint:, body: nil, serializer: @serializer, parser: HTTParty::Parser, content_type: CONTENT_TYPE,
                   accept: ACCEPT)
          perform_signed_request method: Net::HTTP::Delete, endpoint: endpoint, body: body, serializer: serializer,
                                 parser: parser, content_type: content_type, accept: accept
        end

        # Loads or generates a private key
        #
        # @return [OpenSSL::PKey::RSA]
        def private_key
          @private_key ||= OpenSSL::PKey::RSA.new(@private_key_pem || key_size)
        end

        # Public key
        #
        # @return [OpenSSL::PKey::RSA]
        def public_key
          private_key.public_key
        end

        # Host
        #
        # @return [String]
        def host
          @host ||= uri.host
        end

        # If the endpoint is on a subdirectory, we need the absolute
        # path for signing
        #
        # @param :endpoint [String]
        # @return [String]
        def absolute_endpoint(endpoint)
          "#{uri.path}/#{endpoint}".squeeze('/')
        end

        # Absolute URL for an endpoint
        #
        # @param :endpoint [String]
        # @return [String]
        def absolute_url(endpoint)
          uri.dup.tap do |u|
            u.path = absolute_endpoint(endpoint)
          end.to_s
        end

        private

        # Perform request
        #
        # @param method [Net::HTTP::Get,Net::HTTP::Post,Net::HTTP::Put,Net::HTTP::Delete]
        # @param endpoint [String]
        # @param body [Hash]
        # @param serializer [Proc]
        # @param parser [HTTParty::Parser,Proc]
        # @param content_type [String]
        # @param accept [Array<String>]
        # @return [HTTParty::Response]
        def perform_signed_request(method:, endpoint:, body: nil, serializer: @default_serializer, parser: HTTParty::Parser,
                                   content_type: CONTENT_TYPE, accept: ACCEPT)
          headers = default_headers(content_type: content_type, accept: accept)

          unless body.nil?
            body = serializer.call(body)
            checksum_body!(body, headers)
          end

          # Mimics HTTParty.perform_request, but processing the header
          # after the request is instantiated. No need to process
          # cookies for now. Uses the public key URL as a caching key so
          # we revalidate access on shared caches.
          options = { body: body, headers: headers, base_uri: url, parser: parser, stream_body: true, timeout: 5 }
          options = HTTParty::ModuleInheritableAttributes.hash_deep_dup(self.class.default_options).merge(options)
          response_body = ''.dup

          HTTParty::Request.new(method, endpoint, options).tap do |request|
            headers.request = request
          end.perform do |fragment|
            fragment_to_body!(fragment, response_body)
          end.tap do |response|
            # Brotli decompression is delayed until after full download
            if response.headers['content-encoding'] == 'br'
              ''.dup.tap do |inflated_response_body|
                inflate_body!(response_body, inflated_response_body)
                response_body = inflated_response_body
              end
            end

            next if response_body.empty?

            fix_response!(response, response_body)
          end
        end

        # Default headers
        #
        # @param :content_type [String]
        # @param :accept [Array<String>]
        # @return [SignedHeaders]
        def default_headers(content_type: CONTENT_TYPE, accept: ACCEPT)
          SignedHeaders[
            'User-Agent' => "DistributedPress/#{DistributedPress::VERSION}",
            'Content-Type' => content_type,
            'Accept' => accept.join(', '),
            'Host' => host,
            'Accept-Encoding' => "#{'br;q=1.0,' if brotli?}gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
            'Date' => Time.now.utc.httpdate
          ].tap do |headers|
            headers.private_key = private_key
            headers.public_key_url = public_key_url
          end
        end

        # Generate a checksum for the request body
        #
        # @param :body [String]
        # @param :headers [Hash]
        def checksum_body!(body, headers)
          headers['Digest'] = "SHA-256=#{Digest::SHA256.base64digest body}"

          nil
        end

        # Brotli available
        def brotli?
          begin
            require 'brs'
          rescue LoadError => e
            if logger
              logger.warn e.message
            else
              puts e
            end
          end

          defined? ::BRS
        end

        # Inflates a Brotli compressed fragment.  A fragment could be
        # small enough to contain a huge inflated payload.  In our
        # tests, 2GB of zeroes were compressed to a 1.6KB fragment.
        #
        # @todo Maybe each_char is too slow?
        # @param body [String]
        # @param append_to [String]
        def inflate_body!(body, append_to)
          body_io = StringIO.new(body)

          BRS::Stream::Reader.new(body_io, source_buffer_length: 256,
                                           destination_buffer_length: 1024).each_char do |char|
            append_to << char

            if append_to.bytesize > 1_000_000
              raise ContentTooLargeError,
                    "Content too large after inflating, stopped at #{append_to.bytesize} bytes!"
            end
          end
        end

        # Collect fragments into body by checking the bytesize and
        # failing if it gets too big.
        #
        # @param :fragment [HTTParty::ResponseFragment]
        # @param :append_to [String]
        def fragment_to_body!(fragment, append_to)
          if fragment.http_response['content-encoding'] == 'br' && !brotli?
            raise BrotliUnsupportedError, 'Server sent brotli-encoded response but ruby-brs gem is missing'
          end

          append_to << fragment

          return unless append_to.bytesize > 1_000_000

          raise ContentTooLargeError,
                "Content too large, stopped at #{append_to.bytesize} bytes!"
        end

        # Re-adds a streamed body to the response and caches it again so
        # it can carry the body
        #
        # @param response [HTTParty::Response]
        # @param body [String]
        def fix_response!(response, body)
          request = response.request
          parser = request.options[:parser]
          format = request.format || :plain

          response.instance_variable_set(:@body, body)
          response.instance_variable_set(:@parsed_block, -> { parser.call(body, format) })
          response.instance_variable_set(:@parsed_response, nil)

          self.class.cache_store.set(response.cache_key, response)
        end
      end
    end
  end
end
