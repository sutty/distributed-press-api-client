# frozen_string_literal: true

require_relative 'referenced_object'

class DistributedPress
  module V1
    module Social
      # A Collection or OrderedCollection is a an object with a list of
      # items. Sometimes, collections are paginated, so we provide
      # methods to traverse them.
      class Collection < ReferencedObject
        include Enumerable

        ATTRIBUTES = %w[items orderedItems].freeze
        RECURSIVE_ATTRIBUTES = %w[first next].freeze

        # If the Collection has items, yield them and keep processing
        # any pages forward until the end.
        #
        # @todo Implement lazy loading
        def each(&block)
          counter = 0
          total_items = object['totalItems']

          Enumerator.new do |enum|
            catch :total_items_reached do
              ATTRIBUTES.each do |attribute|
                next unless object.key?(attribute)

                referenced[attribute].each(&block).each do |item|
                  counter += 1

                  enum << item

                  if total_items
                    throw :total_items_reached if total_items == counter
                    throw :total_items_reached if counter > 1000
                  end
                end
              end

              RECURSIVE_ATTRIBUTES.each do |attribute|
                next unless object.key?(attribute)
                throw :total_items_reached if object['id'] && object['id'] == referenced[attribute]['id']

                referenced[attribute].each(&block).each do |item|
                  counter += 1
                  enum << item

                  if total_items
                    throw :total_items_reached if total_items == counter
                    throw :total_items_reached if counter > 1000
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
