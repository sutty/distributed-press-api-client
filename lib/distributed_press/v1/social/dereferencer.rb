# frozen_string_literal: true

require 'addressable'
require_relative 'reference'
require_relative 'referenced_object'
require_relative 'collection'

class DistributedPress
  module V1
    module Social
      # Fetches ActivityStreams from different instances by
      # instantiating clients.
      class Dereferencer
        # @return [DistributedPress::V1::Social::Client]
        attr_reader :client

        # We only need the client
        def _dump(_)
          Marshal.dump(client)
        end

        def self._load(client)
          new(client: Marshal.load(client))
        end

        # @param :client [DistributedPress::V1::Social::Client]
        def initialize(client:)
          @client = client
          # @todo Replace with a class
          @parser =
            proc do |body, format|
              next HTTParty::Parser.call(body, :html) if body&.start_with? '<'
              next HTTParty::Parser.call(body, format || :plain) unless body&.start_with? '{'

              object = HTTParty::Parser.call(body, :json)

              # @todo Actually validate objects
              case object['type']
              when 'Collection', 'OrderedCollection', 'CollectionPage', 'OrderedCollectionPage'
                Collection.new(object: object, dereferencer: self)
              else
                ReferencedObject.new(object: object, dereferencer: self)
              end
            end
        end

        # Fetch a URI, if it's an HTML page, look for the alternate
        # version so we always have the correct activity.
        #
        # @todo Raise an error if the content-type is not supported?
        # @param :uri [String, Addressable::URI]
        # @return [HTTParty::Response]
        def get(uri:)
          original_uri = uri
          response = nil
          counter = 0

          loop do
            break if counter > 3

            uri = uris(uri)
            client = clients(uri)
            response = client.get(endpoint: uri.request_uri, parser: @parser)

            # Break the loop if the request failed
            break unless response.success?
            # Or if it's not an HTML document
            break unless response.parsed_response.is_a?(::Nokogiri::HTML5::Document)

            link = response.parsed_response.css('link[rel=alternate]').find do |link|
              next unless link['type']
              next true if client.class::ACCEPT.include?(link['type'])

              client.class::ACCEPT.any? do |accept|
                link['type'].start_with? accept
              end
            end

            break if link.nil?
            break if link['href'].nil?
            break if link['href'].empty?
            break if link['href'] == uri

            # Start loop again with the new URI
            counter += 1
            uri = link['href']
          end

          response.tap do |r|
            raise(client.class::EmptyResponseError, original_uri) unless r
          end
        end

        # Gets a client for a URI
        #
        # @param :uri [Addressable::URI]
        # @return [DistributedPress::V1::Social::Client]
        def clients(uri)
          @@clients ||= {}
          @@clients[uri.origin] ||=
            client.class.new(
              url: uri.origin,
              public_key_url: client.public_key_url,
              private_key_pem: client.private_key.to_s,
              logger: client.logger,
              cache_store: client.class.cache_store
            )
        end

        # Gets a reference for a URI and indexes it by the complete
        # and normalized URI
        #
        # @param :uri [String, Addressable::URI]
        # @return [DistributedPress::V1::Social::Reference]
        def references(uri)
          @@references ||= {}
          @@references[uri.to_s] ||= Reference.new(uri: uri.to_s, dereferencer: self)
        end

        # Make sure we're getting a normalized Addressable::URI
        #
        # @param :uri [String, Addressable::URI]
        # @return [Addressable::URI]
        def uris(uri)
          @@uris ||= {}
          @@uris[uri.to_s] ||=
            (if uri.is_a? Addressable::URI
               uri
             else
               Addressable::URI.parse(uri)
             end).normalize
        end

      end
    end
  end
end
