# frozen_string_literal: true

require 'uri'
require_relative 'client'

class DistributedPress
  module V1
    module Social
      # Manages the actor's followers on the Social Inbox
      class Followers
        # @return [DistributedPress::V1::Social::Client]
        attr_reader :client

        # @return [String]
        attr_reader :actor

        # @param :client [DistributedPress::V1::Social::Client]
        # @param :actor [String]
        def initialize(client:, actor:)
          @client = client
          @actor = actor
        end

        # Get the actor's inbox
        #
        # @return [HTTParty::Response]
        def get
          client.get(endpoint: endpoint)
        end

        # 
        #
        # @param :id [String] Activity ID
        # @return [HTTParty::Response]
        def remove(id:)
          client.delete(endpoint: "#{endpoint}/#{URI.encode_uri_component(id)}")
        end

        # Followers
        #
        # @return [String]
        def endpoint
          @endpoint ||= "/v1/#{actor}/followers"
        end
      end
    end
  end
end
