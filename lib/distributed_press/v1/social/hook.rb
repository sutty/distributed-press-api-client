# frozen_string_literal: true

require_relative 'client'
require_relative 'schemas/webhook'

class DistributedPress
  module V1
    module Social
      # Manages the actor's hooks on the Social Inbox
      class Hook
        class Error < StandardError; end
        class ValidationError < Error; end
        class EventNotValidError < Error; end

        ACCEPT = %w[text/plain].freeze
        CONTENT_TYPE = 'text/plain'
        EVENTS = %w[moderationqueued onapproved onrejected].freeze

        # @return [DistributedPress::V1::Social::Client]
        attr_reader :client

        # @return [String]
        attr_reader :actor

        # @param :client [DistributedPress::V1::Social::Client]
        # @param :actor [String]
        def initialize(client:, actor:)
          @client = client
          @actor = actor
          # The serializer validates the body format and raises an
          # exception if it contains errors.
          @serializer = proc do |body|
            body.tap do |b|
              next if b.errors.empty?

              raise ValidationError, body.errors.to_h.map do |key, messages|
                messages.map do |message|
                  "#{key} #{message}"
                end
              end.flatten.join(', ')
            end.to_h.to_json
          end

          # XXX: format is nil but should be :json
          @parser = proc do |body, format|
            next HTTParty::Parser.call(body, format || :plain) unless body.start_with? '{'

            json = HTTParty::Parser.call(body, :json)

            DistributedPress::V1::Social::Schemas::Webhook.new.call(json)
          end
        end

        # Gets a hook and validates return, or 404 when not found
        #
        # @param :event [String]
        # @return [HTTParty::Response]
        def get(event:)
          validate_event! event

          client.get(endpoint: "#{endpoint}/#{event}", parser: @parser)
        end

        # Creates a webhook
        #
        # @param :event [String]
        # @param :hook [DistributedPress::V1::Social::Schemas::Webhook]
        # @return [HTTParty::Response]
        def put(event:, hook:)
          validate_event! event

          client.put(endpoint: "#{endpoint}/#{event}", body: hook, serializer: @serializer, parser: @parser)
        end

        # Removes a hook for an event
        #
        # @param :event [String]
        # @return [HTTParty::Response]
        def delete(event:)
          validate_event! event

          client.delete(endpoint: "#{endpoint}/#{event}", serializer: @serializer, parser: @parser)
        end

        # Endpoint
        #
        # @return [String]
        def endpoint
          @endpoint ||= "/v1/#{actor}/hooks"
        end

        private

        def validate_event!(event)
          raise EventNotValidError, "#{event} must be one of #{EVENTS.join(', ')}" unless EVENTS.include? event
        end
      end
    end
  end
end
