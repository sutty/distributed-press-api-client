# frozen_string_literal: true

require 'base64'
require_relative 'client'

class DistributedPress
  module V1
    module Social
      # Manages the actor's inbox on the Social Inbox
      class Inbox
        # @return [DistributedPress::V1::Social::Client]
        attr_reader :client

        # @return [String]
        attr_reader :actor

        # @param :client [DistributedPress::V1::Social::Client]
        # @param :actor [String]
        def initialize(client:, actor:)
          @client = client
          @actor = actor
        end

        # Creates a Social Inbox by uploading the keypair to it.
        #
        # @param actor_url [String] The URL where the Actor profile is hosted
        # @param announce [Boolean] Automatically announce new accounts
        # @param manually_approves_followers [Boolean] Automatically accept follow requests
        # @return [HTTParty::Response]
        def create(actor_url, announce: false, manually_approves_followers: false)
          inbox_body = {
            'actorUrl' => actor_url,
            'publicKeyId' => "#{actor_url}#main-key",
            'announce' => announce,
            'manuallyApprovesFollowers' => manually_approves_followers,
            'keypair' => {
              'publicKeyPem' => client.public_key.public_to_pem,
              'privateKeyPem' => client.private_key.export
            }
          }

          client.post(endpoint: "/v1/#{actor}", body: inbox_body)
        end

        # Get the actor's inbox
        #
        # @return [HTTParty::Response]
        def get
          client.get(endpoint: endpoint)
        end

        # Send an activity to the actor's inbox. This is typically done
        # by other actors though, not ourselves, so it could be used to
        # send directly to another Actor's Social Inbox.
        #
        # @param :activity [Hash]
        # @return [HTTParty::Response]
        def post(activity:)
          client.post(endpoint: endpoint, body: activity)
        end

        # Reject an activity queued on the inbox
        #
        # @param :id [String] Activity ID
        # @return [HTTParty::Response]
        def reject(id:)
          client.delete(endpoint: "#{endpoint}/#{Base64.encode64(id).delete("\n")}")
        end

        # Accept an activity queued on the inbox
        #
        # @param :id [String] Activity ID
        # @return [HTTParty::Response]
        def accept(id:)
          client.post(endpoint: "#{endpoint}/#{Base64.encode64(id).delete("\n")}", body: {})
        end

        # Inbox
        #
        # @return [String]
        def endpoint
          @endpoint ||= "/v1/#{actor}/inbox"
        end
      end
    end
  end
end
