# frozen_string_literal: true

require 'uri'
require_relative 'client'

class DistributedPress
  module V1
    module Social
      # Manages the actor's outbox on the Social Inbox
      class Outbox
        # @return [DistributedPress::V1::Social::Client]
        attr_reader :client

        # @return [String]
        attr_reader :actor

        # @param :client [DistributedPress::V1::Social::Client]
        # @param :actor [String]
        def initialize(client:, actor:)
          @client = client
          @actor = actor
        end

        # Send an activity to the actor's outbox.  The Social Inbox will
        # take care of sending it to the audiences.
        #
        # @param :activity [Hash]
        # @return [HTTParty::Response]
        def post(activity:)
          client.post(endpoint: endpoint, body: activity)
        end

        # Get an activity queued on the outbox
        #
        # @param :id [String] Activity ID
        # @return [HTTParty::Response]
        def get(id:)
          client.get(endpoint: "#{endpoint}/#{URI.encode_uri_component(id)}")
        end

        # Outbox
        #
        # @return [String]
        def endpoint
          @endpoint ||= "/v1/#{actor}/outbox"
        end
      end
    end
  end
end
