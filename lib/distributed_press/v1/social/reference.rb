# frozen_string_literal: true

class DistributedPress
  module V1
    module Social
      # A lazy loaded reference to a remote object that can access its
      # attributes directly.
      class Reference
        extend Forwardable

        # Public addressing doesn't lead to an activity
        #
        # @return [String]
        PUBLIC = 'https://www.w3.org/ns/activitystreams#Public'

        # @return [String]
        attr_reader :uri

        # @return [DistributedPress::V1::Social::Dereferencer]
        attr_reader :dereferencer

        # @param :uri [String]
        # @param :dereferencer [DistributedPress::V1::Social::Dereferencer]
        # @param :object [ReferencedObject,nil]
        def initialize(uri:, dereferencer:, object: nil)
          @object = object
          @uri = uri
          @dereferencer = dereferencer
        end

        def ==(other)
          case other
          when String then uri == other
          when Reference then uri == other.uri
          else false
          end
        end


        # Fetches the remote object once
        #
        # @return [HTTParty::Response]
        def object
          @object ||= dereferencer.get(uri: uri)
        end

        def inspect
          "#{self.class.name}(#{uri})"
        end

        def_delegators :object, :[], :dig, :to_h, :to_json, :slice, :key?, :keys, :each, :public?, :unlisted?, :private?
      end
    end
  end
end
