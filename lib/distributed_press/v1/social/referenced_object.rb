# frozen_string_literal: true

class DistributedPress
  module V1
    module Social
      # An object with external references
      #
      # @todo Merge with Reference
      class ReferencedObject
        extend Forwardable

        # Public addressing doesn't lead to an activity
        #
        # @return [String]
        PUBLIC = 'https://www.w3.org/ns/activitystreams#Public'

        # Mastodon hides self-replies
        URI_FIXES = %r{&?only_other_accounts=true&?}
        REFERENTIABLE_ATTRIBUTES =
          %w[
            actor
            owner
            attributedTo
            cc
            inReplyTo
            object
            replies
            likes
            shares
            to
            publicKey
            audience

            alsoKnownAs
            devices
            featured
            featuredTags
            followers
            following
            inbox
            movedTo
            outbox

            first
            items
            next
            orderedItems
            partOf
            prev
          ].freeze

        attr_reader :object
        attr_reader :dereferencer
        attr_reader :referenced

        def_delegators :referenced, :[], :dig, :to_h, :to_json, :keys, :key?, :slice

        def initialize(object:, dereferencer:, referenced: nil)
          @object = object
          @dereferencer = dereferencer
          @referenced = referenced || reference_object(object)
        end

        # Is this a public post? A post is public when it's addressed to
        # the public or doesn't have a recipient (like Like)
        #
        # @return [Bool]
        def public?
          recipients = [slice(*%w[audience to]).values].flatten

          recipients.empty? || recipients.include?(PUBLIC)
        end

        # Is it unlisted?
        #
        # @return [Bool]
        def unlisted?
          [self['cc']].flatten.include? PUBLIC
        end

        # It's not publicly available
        #
        # @return [Bool]
        def private?
          !public? && !unlisted?
        end

        def _dump(_)
          Marshal.dump([object, dereferencer])
        end

        def self._load(array)
          object, dereferencer = Marshal.load(array)

          new(object: object, dereferencer: dereferencer)
        end

        def success?
          true
        end

        def parsed_response
          self
        end

        private

        def reference_object(object)
          case object
          when Array
            object.map do |sub_object|
              reference_object sub_object
            end
          when Hash
            new_object = object.dup

            new_object.each_key do |attribute|
              next unless REFERENTIABLE_ATTRIBUTES.include? attribute

              new_object[attribute] = reference_object object[attribute]
            end

            # XXX: Embedded objects may not have an ID, but they do have
            # a type.
            if new_object['type']
              new_object =
                case new_object['type']
                when 'Collection', 'OrderedCollection', 'CollectionPage', 'OrderedCollectionPage'
                  Collection.new(object: object, referenced: new_object, dereferencer: dereferencer)
                else
                  ReferencedObject.new(object: object, referenced: new_object, dereferencer: dereferencer)
                end

              Reference.new(dereferencer: dereferencer, object: new_object, uri: new_object['id'])
            else
              new_object
            end
          when String
            if object == PUBLIC
              object
            else
              dereferencer.references(dereferencer.uris(object.sub(URI_FIXES, '')))
            end
          else
            object
          end
        end
      end
    end
  end
end
