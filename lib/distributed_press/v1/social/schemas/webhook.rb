# frozen_string_literal: true

require 'dry-schema'
require_relative '../../../../dry/schema/processor_decorator'
require_relative '../../../../dry/schema/result_decorator'

class DistributedPress
  module V1
    module Social
      # WebhookSchema
      module Schemas
        class Webhook < Dry::Schema::JSON
          METHODS = %w[GET POST PUT DELETE].freeze

          define do
            required(:url).value(:uri_rfc3986?)
            required(:method).value(included_in?: METHODS)
            required(:headers).hash
          end
        end
      end
    end
  end
end
