# frozen_string_literal: true

require 'base64'
require_relative 'client'

class DistributedPress
  module V1
    module Social
      # Manages the activity's shares (boosts, announces)
      class Shares
        # @return [DistributedPress::V1::Social::Client]
        attr_reader :client

        # @return [String]
        attr_reader :actor

        # @return [String] Activity ID (URL)
        attr_reader :activity

        # @param :client [DistributedPress::V1::Social::Client]
        # @param :actor [String]
        # @param :activity [String]
        def initialize(client:, actor:, activity:)
          @client = client
          @actor = actor
          @activity = activity
        end

        # Get the activities replies collection. Authenticated requests
        # contain the list of replies.
        #
        # @return [HTTParty::Response]
        def get
          client.get(endpoint: endpoint)
        end

        # Replies
        #
        # @return [String]
        def endpoint
          @endpoint ||=
            begin
              encoded_id = Base64.encode64(activity).delete("\n")

              if encoded_id.size > 256
                raise DistributedPress::V1::Social::Client::ActivityIdTooLargeError.new("Base64 encoding too large (256 char limit): #{activity}")
              end

              "/v1/#{actor}/inbox/shares/#{encoded_id}"
            end
        end
      end
    end
  end
end
