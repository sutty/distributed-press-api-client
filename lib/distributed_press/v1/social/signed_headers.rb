# frozen_string_literal: true

class DistributedPress
  module V1
    module Social
      # Headers need to be signed by knowing the HTTP method (verb) and
      # path.  Since we're using caching, we need to change the
      # signature after it's changed.
      class SignedHeaders < Hash
        # Signing algorithm
        #
        # @todo is it possible to use other algorithms?
        ALGORITHM = 'rsa-sha256'

        # Required by HTTP Signatures
        REQUEST_TARGET = '(request-target)'

        # Headers included in the signature
        # rubocop:disable Style/MutableConstant
        SIGNABLE_HEADERS = [REQUEST_TARGET, 'Host', 'Date', 'Digest']
        # rubocop:enable Style/MutableConstant

        # @return [HTTParty::Request]
        attr_accessor :request

        # @return [OpenSSL::PKey::RSA]
        attr_accessor :private_key

        # @return [String]
        attr_accessor :public_key_url

        # Takes advantage of HTTParty::Request.setup_raw_request running
        # to_hash on the headers, so we sign as soon as we're ready to
        # send the request.
        #
        # @return [Hash]
        def to_hash
          request_target!
          sign_headers!

          # xxx: converts to an actual Hash to facilitate marshaling
          super.to_h
        end

        private

        # @return [String]
        def verb
          request.http_method.name.split('::').last.downcase
        end

        def request_target!
          self[REQUEST_TARGET] = "#{verb} #{request.path}"
        end

        # HTTP Signatures
        #
        # @see {https://docs.joinmastodon.org/spec/security/}
        # @param :headers [Hash]
        def sign_headers!
          self['Signature'] = {
            'keyId' => public_key_url,
            'algorithm' => ALGORITHM,
            'headers' => signable_headers,
            'signature' => signed_headers
          }.map do |key, value|
            "#{key}=\"#{value}\""
          end.join(',')

          delete REQUEST_TARGET

          nil
        end

        # List of headers to be signed, removing headers that don't
        # exist on the request.
        #
        # @return [String]
        def signable_headers
          (SIGNABLE_HEADERS & keys).join(' ').downcase
        end

        # Sign headers
        #
        # @return [String]
        def signed_headers
          Base64.strict_encode64(
            private_key.sign(
              OpenSSL::Digest.new('SHA256'),
              signature_content
            )
          )
        end

        # Generates a string to be signed
        #
        # @return [String]
        def signature_content
          slice(*SIGNABLE_HEADERS).map do |key, value|
            "#{key.downcase}: #{value}"
          end.join("\n")
        end
      end
    end
  end
end
