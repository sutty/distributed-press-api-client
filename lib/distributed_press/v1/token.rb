# frozen_string_literal: true

require 'jwt'
require_relative 'errors'
require_relative 'schemas/token_header'
require_relative 'schemas/token_payload'

class DistributedPress
  module V1
    # Authentication is done using a JWT with different capabilities.
    # This class decodes and help us figure out what we can do
    # client-side.
    class Token
      # Decoded JWT
      #
      # @return [JWT]
      attr_reader :decoded

      # Parsed and validated token header
      #
      # @see https://jwt.io
      # @return [DistributedPress::V1::Schemas::TokenHeader]
      attr_reader :header

      # Parsed and validated token payload
      #
      # @return [DistributedPress::V1::Schemas::TokenPayload]
      attr_reader :payload

      # Decodes a JWT string
      #
      # @param token [String]
      def initialize(token:)
        @token = token
        # XXX: We can't validate the token without its secret.
        @decoded = JWT.decode(token, nil, false)

        @header = Schemas::TokenHeader.new.call(decoded.find do |part|
          part['alg']
        end)

        @payload = Schemas::TokenPayload.new.call(decoded.find do |part|
          part['tokenId']
        end)

        raise TokenHeaderNotValidError unless header.success?
        raise TokenPayloadNotValidError unless payload.success?
      end

      # Returns the original token when converted to String
      #
      # @return [String]
      def to_s
        @token
      end

      # Checks if the token expired.  Returns false if expiration time
      # is negative.
      #
      # @return [Boolean]
      def expired?
        return false if forever?

        !expires_in_x_seconds.positive?
      end

      # Checks if the token never expires
      #
      # @return [Boolean]
      def forever?
        payload[:expires].negative?
      end

      # Return issuing time
      #
      # @return [Time]
      def issued_at
        Time.at(payload[:iat])
      end

      # Return expiration time
      #
      # @return [Time]
      def expires_at
        Time.at(0, payload[:expires], :millisecond)
      end

      # Returns expiration time in seconds
      #
      # @return [Integer]
      def expires_in_x_seconds
        expires_at.to_i - Time.now.to_i
      end

      # Checks if a token gives us publisher capabilities
      #
      # @return [Boolean]
      def publisher?
        payload[:capabilities].include? 'publisher'
      end

      # Checks if a token gives us admin capabilities
      #
      # @return [Boolean]
      def admin?
        payload[:capabilities].include? 'admin'
      end

      # Checks if a token gives us refresh capabilities
      #
      # @return [Boolean]
      def refresh?
        payload[:capabilities].include? 'refresh'
      end

      # Returns payload capabilities
      #
      # @return [Array]
      def capabilities
        payload[:capabilities]
      end
    end
  end
end
