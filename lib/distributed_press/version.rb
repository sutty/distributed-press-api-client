# frozen_string_literal: true

# API client
class DistributedPress
  # Version
  VERSION = '0.5.2'
end
