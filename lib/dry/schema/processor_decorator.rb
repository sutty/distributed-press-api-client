# frozen_string_literal: true

require 'dry/schema/processor'

module Dry
  module Schema
    # Include the processor as Result option so we can deserialize
    # later.
    module ProcessorDecorator
      def self.included(base)
        base.class_eval do
          # Add the processor to the result so we can deserialize it
          # later
          def call(input)
            Result.new(input.dup, message_compiler: message_compiler, processor: self) do |result|
              steps.call(result)
            end
          end
        end
      end
    end
  end
end

Dry::Schema::Processor.include Dry::Schema::ProcessorDecorator
