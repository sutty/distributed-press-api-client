# frozen_string_literal: true

require 'dry/schema/processor'

module Dry
  module Schema
    # This decorator allows to serialize Dry::Schema::Result objects
    # by storing the data and processor so they can be run again during
    # deserialization.
    module ResultDecorator
      def self.included(base)
        base.class_eval do
          option :processor

          def _dump(_)
            Marshal.dump(
              {
                processor: processor.class,
                data: to_h
              }
            )
          end

          def self._load(data)
            data = Marshal.load(data)

            data[:processor].new.call(data[:data])
          end
        end
      end
    end
  end
end

Dry::Schema::Result.include Dry::Schema::ResultDecorator
