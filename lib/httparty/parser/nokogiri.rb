# frozen_string_literal: true

require 'httparty/parser'
require 'nokogiri'

module HTTParty
  class Parser
    # Parse HTML as Nokogiri documents
    module Nokogiri
      protected

      def html
        ::Nokogiri::HTML5(body)
      end
    end
  end
end

HTTParty::Parser.class_eval do
  prepend HTTParty::Parser::Nokogiri
end
