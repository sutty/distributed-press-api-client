# frozen_string_literal: true

require 'distributed_press'

Jekyll::Hooks.register :site, :post_write, priority: :low do |site|
  def logger(message:, level: :info)
    Jekyll.logger.public_send(level, 'Distributed Press:', message)
  end

  def warn(message)
    logger level: :warn, message: message
  end

  def debug(message)
    logger level: :debug, message: message
  end

  def info(message)
    logger message: message
  end

  unless ENV['JEKYLL_ENV'] == 'production'
    debug "Ignoring publication in #{ENV['JEKYLL_ENV']} mode"
    next
  end

  # Compatibility with jekyll-locales, only publish on the last locale
  dest = site.dest
  locales_enabled = site.config['plugins'].include?('jekyll-locales')

  if locales_enabled && site.locales?
    if site.locale != site.locales.last
      info "Ignoring #{site.locale}"
      next
    else
      dest = File.realpath(File.join(site.dest, '..'))
    end
  end

  hostname   = ENV['DISTRIBUTED_PRESS_PROJECT_DOMAIN']
  hostname ||= site.config['hostname']
  hostname ||= URI(site.config['url']).hostname

  raise URI::Error unless hostname

  info 'Publishing website'

  distributed_press = DistributedPress.new(url: ENV['DISTRIBUTED_PRESS_API_URL'], project_domain: hostname)

  if distributed_press.configure
    if distributed_press.publish(dest)
      info 'Published!'
    else
      warn distributed_press.publish_result['error']
    end
  else
    warn distributed_press.configure_result['error']
  end
rescue URI::Error
  warn '`hostname` or `url` keys missing on _config.yml, skipping publication.'
end
