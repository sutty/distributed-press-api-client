# frozen_string_literal: true

FactoryBot.define do
  factory :admin, class: 'DistributedPress::V1::Schemas::Admin' do
    id do
      Nanoid.generate
    end

    name do
      Nanoid.generate
    end

    initialize_with do
      new.call(id: id, name: name)
    end
  end
end
