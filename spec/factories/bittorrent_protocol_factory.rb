# frozen_string_literal: true

FactoryBot.define do
  factory :bittorrent_protocol, class: 'DistributedPress::V1::Schemas::BittorrentProtocol' do
    enabled do
      [true, false].sample
    end

    link do
      'https://pinata.cloud'
    end

    gateway do
      'https://ipfs.distributed.press/sutty.ml'
    end

    dnslink do
      '/bt/example-raw'
    end

    infoHash do
      'bittorrent://example-link-infoHash'
    end

    pubKey do
      SecureRandom.hex
    end

    magnet do
      'magnet:?xt:urn:btih:example-link&xs=urn:btpk:example-link'
    end

    initialize_with do
      new.call(enabled: enabled, link: link, gateway: gateway, infoHash: infoHash, pubKey: pubKey, dnslink: dnslink,
               magnet: magnet)
    end
  end
end
