# frozen_string_literal: true

FactoryBot.define do
  factory :http_protocol, class: 'DistributedPress::V1::Schemas::HttpProtocol' do
    enabled do
      [true, false].sample
    end

    link do
      'https://pinata.cloud'
    end

    initialize_with do
      new.call(enabled: enabled, link: link)
    end
  end
end
