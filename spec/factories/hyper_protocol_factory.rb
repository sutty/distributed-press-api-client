# frozen_string_literal: true

FactoryBot.define do
  factory :hyper_protocol, class: 'DistributedPress::V1::Schemas::HyperProtocol' do
    enabled do
      [true, false].sample
    end

    link do
      'https://pinata.cloud'
    end

    gateway do
      'https://hyper.distributed.press/sutty.ml'
    end

    raw do
      SecureRandom.hex
    end

    initialize_with do
      new.call(enabled: enabled, link: link, gateway: gateway, raw: raw)
    end
  end
end
