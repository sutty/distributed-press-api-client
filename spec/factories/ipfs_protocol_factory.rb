# frozen_string_literal: true

FactoryBot.define do
  factory :ipfs_protocol, class: 'DistributedPress::V1::Schemas::IpfsProtocol' do
    enabled do
      [true, false].sample
    end

    link do
      'https://pinata.cloud'
    end

    gateway do
      'https://ipfs.distributed.press/sutty.ml'
    end

    cid do
      SecureRandom.hex
    end

    pubKey do
      SecureRandom.hex
    end

    initialize_with do
      new.call(enabled: enabled, link: link, gateway: gateway, cid: cid, pubKey: pubKey)
    end
  end
end
