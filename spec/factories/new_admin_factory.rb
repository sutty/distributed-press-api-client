# frozen_string_literal: true

FactoryBot.define do
  factory :new_admin, class: 'DistributedPress::V1::Schemas::NewAdmin' do
    name do
      Nanoid.generate
    end

    initialize_with do
      new.call(name: name)
    end
  end
end
