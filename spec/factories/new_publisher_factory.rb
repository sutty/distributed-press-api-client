# frozen_string_literal: true

FactoryBot.define do
  factory :new_publisher, class: 'DistributedPress::V1::Schemas::NewPublisher' do
    name do
      Nanoid.generate
    end

    initialize_with do
      new.call(name: name)
    end
  end
end
