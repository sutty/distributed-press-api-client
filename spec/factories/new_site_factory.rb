# frozen_string_literal: true

FactoryBot.define do
  factory :new_site, class: 'DistributedPress::V1::Schemas::NewSite' do
    domain do
      'sutty.ml'
    end

    public do
      [true, false].sample
    end

    initialize_with do
      new.call(domain: domain, public: public)
    end
  end

  factory :new_site_with_protocols, class: 'DistributedPress::V1::Schemas::NewSite' do
    domain do
      'sutty.ml'
    end

    public do
      [true, false].sample
    end

    protocols do
      {
        http: [true, false].sample,
        ipfs: [true, false].sample,
        hyper: [true, false].sample
      }
    end

    initialize_with do
      new.call(domain: domain, public: public, protocols: protocols)
    end
  end
end
