# frozen_string_literal: true

FactoryBot.define do
  factory :new_token_payload, class: 'DistributedPress::V1::Schemas::NewTokenPayload' do
    issuedTo do
      'system'
    end

    capabilities do
      DistributedPress::V1::Schemas::TokenPayload::CAPABILITIES
    end

    initialize_with do
      new.call(capabilities: capabilities, issuedTo: issuedTo)
    end
  end
end
