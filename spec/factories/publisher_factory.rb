# frozen_string_literal: true

FactoryBot.define do
  factory :publisher, class: 'DistributedPress::V1::Schemas::Publisher' do
    id do
      Nanoid.generate
    end

    name do
      Nanoid.generate
    end

    ownedSites do
      (1..(rand(1..10))).map do
        Nanoid.generate
      end
    end

    initialize_with do
      new.call(id: id, name: name, ownedSites: ownedSites)
    end
  end
end
