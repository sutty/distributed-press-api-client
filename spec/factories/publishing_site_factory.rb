# frozen_string_literal: true

FactoryBot.define do
  factory :publishing_site, class: 'DistributedPress::V1::Schemas::PublishingSite' do
    id do
      Nanoid.generate
    end

    initialize_with do
      new.call(id: id)
    end
  end
end
