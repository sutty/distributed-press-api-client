# frozen_string_literal: true

FactoryBot.define do
  factory :site, class: 'DistributedPress::V1::Schemas::Site' do
    id do
      Nanoid.generate
    end

    public do
      [true, false].sample
    end

    domain do
      'sutty.ml'
    end

    protocols do
      {
        http: [true, false].sample,
        ipfs: [true, false].sample,
        hyper: [true, false].sample,
        bittorrent: [true, false].sample
      }
    end

    links do
      {
        http: build(:http_protocol).to_h,
        hyper: build(:hyper_protocol).to_h,
        ipfs: build(:ipfs_protocol).to_h,
        bittorrent: build(:bittorrent_protocol).to_h
      }
    end

    initialize_with do
      new.call(id: id, domain: domain, protocols: protocols, links: links, public: public)
    end
  end
end
