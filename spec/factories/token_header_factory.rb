# frozen_string_literal: true

FactoryBot.define do
  factory :token_header, class: 'DistributedPress::V1::Schemas::TokenHeader' do
    alg do
      'RS256'
    end

    typ do
      'JWT'
    end

    initialize_with do
      new.call(alg: alg, typ: typ)
    end
  end
end
