# frozen_string_literal: true

FactoryBot.define do
  factory :token_payload, class: 'DistributedPress::V1::Schemas::TokenPayload' do
    tokenId do
      Nanoid.generate
    end

    issuedTo do
      'system'
    end

    iat do
      (Time.now - rand(1..12).months.ago).to_i
    end

    expires do
      -1
    end

    capabilities do
      DistributedPress::V1::Schemas::TokenPayload::CAPABILITIES
    end

    initialize_with do
      new.call(tokenId: tokenId, iat: iat, expires: expires, capabilities: capabilities, issuedTo: issuedTo)
    end

    factory :token_payload_admin do
      capabilities do
        %w[admin]
      end
    end

    factory :token_payload_publisher do
      capabilities do
        %w[publisher]
      end
    end

    factory :token_payload_non_publisher do
      capabilities do
        %w[refresh]
      end
    end

    factory :token_payload_non_admin do
      capabilities do
        %w[refresh]
      end
    end

    factory :token_payload_expired do
      expires do
        (Time.now - rand(1..12).months).to_i * 1000
      end
    end

    factory :token_payload_about_to_expire do
      expires do
        (Time.now.to_i + rand(1..59)) * 1000
      end

      factory :token_payload_about_to_expire_non_refresh do
        capabilities do
          %w[publisher]
        end
      end
    end
  end
end
