# frozen_string_literal: true

FactoryBot.define do
  factory :update_site, class: 'DistributedPress::V1::Schemas::UpdateSite' do
    id do
      Nanoid.generate
    end

    public do
      [true, false].sample
    end

    protocols do
      {
        http: [true, false].sample,
        ipfs: [true, false].sample,
        hyper: [true, false].sample
      }
    end

    initialize_with do
      new.call(id: id, protocols: protocols, public: public)
    end
  end
end
