# frozen_string_literal: true

FactoryBot.define do
  factory :webhook, class: 'DistributedPress::V1::Social::Schemas::Webhook' do
    url do
      "https://sutty.nl/#{Nanoid.generate}"
    end

    http_method do
      DistributedPress::V1::Social::Schemas::Webhook::METHODS.sample
    end

    headers do
      rand(10).times.map do
        [SecureRandom.hex, SecureRandom.hex]
      end.to_h
    end

    initialize_with do
      new.call(url: url, method: http_method, headers: headers)
    end
  end
end
