# frozen_string_literal: true

require 'spec_helper'

RSpec.describe DistributedPress do
  def with_modified_env(options = {}, &block)
    ClimateControl.modify(options, &block)
  end

  context 'initialization' do
    it 'requires an api key' do
      with_modified_env DISTRIBUTED_PRESS_API_KEY: nil do
        expect do
          DistributedPress.new
        end.to(raise_error(ArgumentError, /api key/i))
      end
    end

    it 'requires a project domain' do
      with_modified_env DISTRIBUTED_PRESS_API_KEY: 'random', DISTRIBUTED_PRESS_PROJECT_DOMAIN: nil do
        expect do
          DistributedPress.new
        end.to(raise_error(ArgumentError, /project domain/i))
      end
    end

    it 'can be initialized with proper options' do
      expect(DistributedPress.new(api_key: 'random', project_domain: 'random')).to(be_instance_of(DistributedPress))
    end

    it 'can be initialized with env vars' do
      with_modified_env DISTRIBUTED_PRESS_API_KEY: 'random', DISTRIBUTED_PRESS_PROJECT_DOMAIN: 'random' do
        expect(DistributedPress.new).to(be_instance_of(DistributedPress))
      end
    end
  end

  describe '#configure' do
    it 'can send configuration' do
      stub_request(:post, 'https://api.distributed.press/v0/publication/configure')
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: { errorCode: 0 }.to_json
        )

      client = DistributedPress.new(api_key: 'random', project_domain: 'random')

      expect(client.configure).to(eql(true))
    end
  end

  describe '#publish' do
    it 'can send configuration' do
      stub_request(:post, 'https://api.distributed.press/v0/publication/publish')
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: { errorCode: 0 }.to_json
        )

      client = DistributedPress.new(api_key: 'random', project_domain: 'random')

      expect(client.publish(ENV['PWD'])).to(eql(true))
    end
  end
end
