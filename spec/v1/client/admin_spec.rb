# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/client/admin'

RSpec.describe DistributedPress::V1::Client::Admin do
  before do
    payload = build(:token_payload).to_h

    @ecdsa_key = OpenSSL::PKey::EC.generate('prime256v1')
    @token = JWT.encode payload, @ecdsa_key, 'ES256'
  end

  describe '#create' do
    it 'creates a admin' do
      new_admin = build :new_admin
      admin = build :admin, name: new_admin[:name]

      stub_request(:post, 'https://api.distributed.press/v1/admin')
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: admin.to_h.to_json
        )

      client = DistributedPress::V1::Client.new(token: @token)
      admins = DistributedPress::V1::Client::Admin.new(client)

      expect(admins.create(new_admin).to_h).to(eql(admin.to_h))
    end

    it "can't create a admin if we're not admins" do
      payload = build(:token_payload_non_admin).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      admins = DistributedPress::V1::Client::Admin.new(client)

      expect do
        admins.create(build(:new_admin))
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end
  end

  describe '#delete' do
    it 'deletes a admin' do
      admin = build :admin

      stub_request(:delete, "https://api.distributed.press/v1/admin/#{admin[:id]}")
        .to_return(status: 200)

      client = DistributedPress::V1::Client.new(token: @token)
      admins = DistributedPress::V1::Client::Admin.new(client)

      expect(admins.delete(admin)).to(eql(true))
    end

    it "can't delete a admin if we're not admins" do
      payload = build(:token_payload_non_admin).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      admins = DistributedPress::V1::Client::Admin.new(client)

      expect do
        admins.delete(build(:admin))
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end
  end
end
