# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/client/auth'

RSpec.describe DistributedPress::V1::Client::Auth do
  before do
    @payload = build(:token_payload)
    @ecdsa_key = OpenSSL::PKey::EC.generate('prime256v1')
    @token = JWT.encode @payload.to_h, @ecdsa_key, 'ES256'
  end

  describe '#exchange' do
    it 'gets a new token' do
      new_token_payload = build(:new_token_payload, capabilities: @payload[:capabilities],
                                                    issuedTo: @payload[:issuedTo])
      new_token = DistributedPress::V1::Token.new token: JWT.encode(@payload.to_h, @ecdsa_key, 'ES256')

      stub_request(:post, 'https://api.distributed.press/v1/auth/exchange')
        .to_return(
          status: 200,
          body: new_token.to_s
        )

      client = DistributedPress::V1::Client.new(token: @token)
      auth = DistributedPress::V1::Client::Auth.new(client)

      expect(auth.exchange(new_token_payload).to_s).to(eql(new_token.to_s))
    end

    it "can't get a token with more capabilities" do
      payload = build(:token_payload_publisher)
      new_token_payload = build(:new_token_payload, capabilities: %w[admin], issuedTo: payload[:issuedTo])
      token = JWT.encode(payload.to_h, @ecdsa_key, 'ES256')

      stub_request(:post, 'https://api.distributed.press/v1/auth/exchange')
        .to_return(status: 401)

      client = DistributedPress::V1::Client.new(token: token)
      auth = DistributedPress::V1::Client::Auth.new(client)

      expect do
        auth.exchange(new_token_payload)
      end.to(raise_error(DistributedPress::V1::TokenUnauthorizedError))
    end
  end

  describe '#revoke' do
    it 'can revoke tokens' do
      client = DistributedPress::V1::Client.new(token: @token)
      auth = DistributedPress::V1::Client::Auth.new(client)

      stub_request(:delete, "https://api.distributed.press/v1/auth/revoke/#{@payload[:tokenId]}")
        .to_return(status: 200)

      expect(auth.revoke(@payload)).to(eql(true))

      stub_request(:delete, "https://api.distributed.press/v1/auth/revoke/#{@payload[:tokenId]}")
        .to_return(status: 401)

      expect do
        auth.revoke(@payload)
      end.to(raise_error(DistributedPress::V1::TokenUnauthorizedError))
    end
  end
end
