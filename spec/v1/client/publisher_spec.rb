# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/client/publisher'

RSpec.describe DistributedPress::V1::Client::Publisher do
  before do
    payload = build(:token_payload).to_h

    @ecdsa_key = OpenSSL::PKey::EC.generate('prime256v1')
    @token = JWT.encode payload, @ecdsa_key, 'ES256'
  end

  describe '#create' do
    it 'creates a publisher' do
      new_publisher = build :new_publisher
      publisher = build :publisher, name: new_publisher[:name]

      stub_request(:post, 'https://api.distributed.press/v1/publisher')
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: publisher.to_h.to_json
        )

      client = DistributedPress::V1::Client.new(token: @token)
      publishers = DistributedPress::V1::Client::Publisher.new(client)

      expect(publishers.create(new_publisher).to_h).to(eql(publisher.to_h))
    end

    it "can't create a publisher if we're not admins" do
      payload = build(:token_payload_non_publisher).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      publishers = DistributedPress::V1::Client::Publisher.new(client)

      expect do
        publishers.create(build(:new_publisher))
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end
  end

  describe '#delete' do
    it 'deletes a publisher' do
      publisher = build :publisher

      stub_request(:delete, "https://api.distributed.press/v1/publisher/#{publisher[:id]}")
        .to_return(status: 200)

      client = DistributedPress::V1::Client.new(token: @token)
      publishers = DistributedPress::V1::Client::Publisher.new(client)

      expect(publishers.delete(publisher)).to(eql(true))
    end

    it "can't delete a publisher if we're not admins" do
      payload = build(:token_payload_non_publisher).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      publishers = DistributedPress::V1::Client::Publisher.new(client)

      expect do
        publishers.delete(build(:publisher))
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end
  end
end
