# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/client/site'

RSpec.describe DistributedPress::V1::Client::Site do
  before do
    payload = build(:token_payload).to_h

    @ecdsa_key = OpenSSL::PKey::EC.generate('prime256v1')
    @token = JWT.encode payload, @ecdsa_key, 'ES256'
  end

  describe '#show' do
    it 'shows information about the site' do
      site = build :site
      stub_request(:get, "https://api.distributed.press/v1/sites/#{site[:id]}")
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: site.to_h.to_json
        )

      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect(sites.show(site).to_h).to(eql(site.to_h))
    end
  end

  describe '#delete' do
    it 'deletes a website' do
      site = build :site
      stub_request(:delete, "https://api.distributed.press/v1/sites/#{site[:id]}")
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' }
        )

      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect(sites.delete(site)).to(eql(true))
    end

    it 'deletes a website' do
      publishing_site = build :publishing_site
      stub_request(:delete, "https://api.distributed.press/v1/sites/#{publishing_site[:id]}")
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' }
        )

      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect(sites.delete(publishing_site)).to(eql(true))
    end
  end

  describe '#create' do
    it 'creates a site' do
      new_site = build :new_site
      site = build(:site, protocols: {
                     http: true,
                     ipfs: true,
                     hyper: true
                   })

      stub_request(:post, 'https://api.distributed.press/v1/sites')
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: site.to_h.to_json
        )

      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect(sites.create(new_site).to_h).to(eql(site.to_h))
    end

    it "can't create a site unless we're publishers" do
      payload = build(:token_payload_non_publisher).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect do
        sites.create(build(:new_site))
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end

    it "can't create a site unless we're admins" do
      payload = build(:token_payload_non_publisher).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect do
        sites.create(build(:new_site))
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end

    it "can't create a site with a non valid schema" do
      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)
      new_site = build :new_site, domain: nil, public: false

      expect do
        sites.create(new_site)
      end.to(raise_error(DistributedPress::V1::SchemaNotValidError))
    end
  end

  describe '#update' do
    it 'updates a site' do
      update_site = build :update_site
      site = build(:site, id: update_site[:id], public: true, protocols: update_site[:protocols])

      stub_request(:post, "https://api.distributed.press/v1/sites/#{site[:id]}")
        .to_return(
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: site.to_h.to_json
        )

      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect(sites.update(update_site).to_h).to(eql(site.to_h))
    end

    it "can't update a site unless we're publishers" do
      payload = build(:token_payload_non_publisher).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect do
        sites.update(build(:site))
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end

    it "can't update a site with a non valid schema" do
      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)
      site = build :site, domain: nil, public: false

      expect do
        sites.update(site)
      end.to(raise_error(DistributedPress::V1::SchemaNotValidError))
    end
  end

  describe '#publish' do
    it 'publishes a site' do
      publishing_site = build :publishing_site

      stub_request(:put, "https://api.distributed.press/v1/sites/#{publishing_site[:id]}")
        .to_return(status: 200)

      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect(sites.publish(publishing_site, ENV['PWD'])).to(eql(true))
    end

    it "can't publish a site unless we're publishers" do
      payload = build(:token_payload_non_publisher).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'
      client = DistributedPress::V1::Client.new(token: token)
      sites = DistributedPress::V1::Client::Site.new(client)

      expect do
        sites.publish(build(:new_site), ENV['PWD'])
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end

    it "can't publish a site with a non valid schema" do
      client = DistributedPress::V1::Client.new(token: @token)
      sites = DistributedPress::V1::Client::Site.new(client)
      new_site = build(:new_site, domain: nil, public: false)

      expect do
        sites.publish(new_site, ENV['PWD'])
      end.to(raise_error(DistributedPress::V1::SchemaNotValidError))
    end
  end
end
