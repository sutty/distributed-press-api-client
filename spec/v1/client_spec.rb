# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/client'

RSpec.describe DistributedPress::V1::Client do
  before do
    payload = build(:token_payload).to_h

    @ecdsa_key = OpenSSL::PKey::EC.generate('prime256v1')
    @token = JWT.encode payload, @ecdsa_key, 'ES256'
  end

  describe '#new' do
    it 'fails then the token is expired' do
      payload = build(:token_payload_expired).to_h
      token = JWT.encode payload, @ecdsa_key, 'ES256'

      expect do
        DistributedPress::V1::Client.new(token: token)
      end.to(raise_error(DistributedPress::V1::TokenExpiredError))
    end

    it "renews the token if it's about to expire" do
      token = JWT.encode build(:token_payload_about_to_expire).to_h, @ecdsa_key, 'ES256'

      stub_request(:post, 'https://api.distributed.press/v1/auth/exchange')
        .to_return(
          status: 200,
          body: @token.to_s
        )

      client = DistributedPress::V1::Client.new(token: token)

      expect(client.token.to_s).not_to(eql(token.to_s))
    end

    it "doesn't renew the token if it's not about to expire" do
      stub_request(:post, 'https://api.distributed.press/v1/auth/exchange')
        .to_return(
          status: 200,
          body: @token.to_s
        )

      client = DistributedPress::V1::Client.new(token: @token)

      expect(client.token.to_s).to(eql(@token.to_s))
    end

    it "can't renew the token if we don't have the capability" do
      token = JWT.encode build(:token_payload_about_to_expire_non_refresh).to_h, @ecdsa_key, 'ES256'

      expect do
        DistributedPress::V1::Client.new(token: token)
      end.to(raise_error(DistributedPress::V1::TokenCapabilityMissingError))
    end
  end

  describe '#headers' do
    it 'sends user agent and authentication headers' do
      client = DistributedPress::V1::Client.new(token: @token)

      expect(client.headers).to(eql({
                                      'User-Agent' => "DistributedPress/#{DistributedPress::VERSION}",
                                      'Accept' => 'application/json',
                                      'Content-Type' => 'application/json',
                                      'Authorization' => "Bearer #{@token}"
                                    }))
    end
  end
end
