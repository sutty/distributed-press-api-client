# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/admin'

RSpec.describe DistributedPress::V1::Schemas::Admin do
  before do
    @admin = DistributedPress::V1::Schemas::Admin.new
  end

  describe '#call' do
    it 'can load a admin' do
      json = {
        id: Nanoid.generate,
        name: Nanoid.generate
      }

      expect(@admin.call(json).errors.empty?).to(eql(true))
    end

    it 'cannot load a admin without id' do
      json = {
        name: Nanoid.generate
      }

      expect(@admin.call(json).errors.empty?).to(eql(false))
    end

    it 'cannot load a admin without name' do
      json = {
        id: Nanoid.generate
      }

      expect(@admin.call(json).errors.empty?).to(eql(false))
    end
  end
end
