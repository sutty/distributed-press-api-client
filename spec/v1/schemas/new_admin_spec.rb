# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/new_admin'

RSpec.describe DistributedPress::V1::Schemas::NewAdmin do
  before do
    @new_admin = DistributedPress::V1::Schemas::NewAdmin.new
  end

  describe '#call' do
    it 'can load a admin' do
      json = { name: Nanoid.generate }

      expect(@new_admin.call(json).success?).to(eql(true))
    end
  end
end
