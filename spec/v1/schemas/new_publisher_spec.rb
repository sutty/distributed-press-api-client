# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/new_publisher'

RSpec.describe DistributedPress::V1::Schemas::NewPublisher do
  before do
    @new_publisher = DistributedPress::V1::Schemas::NewPublisher.new
  end

  describe '#call' do
    it 'can load a publisher' do
      json = { name: Nanoid.generate }

      expect(@new_publisher.call(json).success?).to(eql(true))
    end
  end
end
