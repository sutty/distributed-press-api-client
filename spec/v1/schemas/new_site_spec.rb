# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/new_site'

RSpec.describe DistributedPress::V1::Schemas::NewSite do
  before do
    @new_site = DistributedPress::V1::Schemas::NewSite.new
  end

  describe '#call' do
    it 'can load a site config' do
      json = {
        domain: 'sutty.ml',
        public: true
      }

      expect(@new_site.call(json).success?).to(eql(true))
    end

    it 'can load a site config with protocols' do
      json = {
        domain: 'sutty.ml',
        public: false,
        protocols: {
          http: true,
          hyper: false,
          ipfs: true
        }
      }

      expect(@new_site.call(json).success?).to(eql(true))
    end

    it 'can load a site without public field' do
      json = { domain: 'sutty.ml',
               protocols: {
                 http: true,
                 hyper: false,
                 ipfs: true
               } }

      expect(@new_site.call(json).success?).to(eql(true))
    end
  end
end
