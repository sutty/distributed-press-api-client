# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/new_token_payload'

RSpec.describe DistributedPress::V1::Schemas::NewTokenPayload do
  before do
    @new_token_payload = DistributedPress::V1::Schemas::NewTokenPayload.new
  end

  describe '#call' do
    it 'can load a new token payload factory' do
      new_token_payload = build :new_token_payload

      expect(new_token_payload.success?).to(eql(true))
    end

    it 'can load a raw new token payload' do
      caps = DistributedPress::V1::Schemas::TokenPayload::CAPABILITIES
      json = {
        issuedTo: 'system',
        capabilities: caps.sample(rand(1..caps.size))
      }

      expect(@new_token_payload.call(json).success?).to(eql(true))
    end
  end
end
