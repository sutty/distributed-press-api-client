# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/publisher'

RSpec.describe DistributedPress::V1::Schemas::Publisher do
  before do
    @publisher = DistributedPress::V1::Schemas::Publisher.new
  end

  describe '#call' do
    it 'can load a publisher' do
      json = {
        id: Nanoid.generate,
        name: Nanoid.generate,
        ownedSites: []
      }

      expect(@publisher.call(json).errors.empty?).to(eql(true))
    end

    it 'cannot load a publisher without id' do
      json = {
        name: Nanoid.generate,
        ownedSites: []
      }

      expect(@publisher.call(json).errors.empty?).to(eql(false))
    end

    it 'cannot load a publisher without name' do
      json = {
        id: Nanoid.generate,
        ownedSites: []
      }

      expect(@publisher.call(json).errors.empty?).to(eql(false))
    end
  end
end
