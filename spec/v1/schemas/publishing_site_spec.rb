# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/publishing_site'

RSpec.describe DistributedPress::V1::Schemas::PublishingSite do
  before do
    @publishing_site = DistributedPress::V1::Schemas::PublishingSite.new
  end

  describe '#call' do
    it 'can load a site config' do
      json = { id: Nanoid.generate }

      expect(@publishing_site.call(json).success?).to(eql(true))
    end
  end
end
