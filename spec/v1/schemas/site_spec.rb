# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/site'

RSpec.describe DistributedPress::V1::Schemas::Site do
  before do
    @site = DistributedPress::V1::Schemas::Site.new
  end

  describe '#call' do
    it 'can load a site config' do
      json = {
        id: Nanoid.generate,
        domain: 'sutty.ml',
        public: false,
        links: {
          http: {
            enabled: true,
            link: 'http://sutty.ml/'
          },
          hyper: {
            enabled: true,
            link: 'hyper://sutty.ml/',
            gateway: 'https://distributed.press/hyper/sutty.ml/',
            raw: 'hyper://465kqqkfg9gon1wf5c373jx6fk46/'
          },
          ipfs: {
            enabled: true,
            link: 'ipns://sutty.ml/',
            gateway: 'https://distributed.press/ipns/sutty.ml/',
            cid: '465kqqkfg9gon1wf5c373jx6fk46',
            pubKey: '465kqqkfg9gon1wf5c373jx6fk46'
          },
          bittorrent: {
            enabled: true,
            link: 'bittorrent://sutty.ml/',
            gateway: 'https://distributed.press/bittorrent/sutty.ml/',
            dnslink: '/bt/example-raw',
            pubKey: '465kqqkfg9gon1wf5c373jx6fk46',
            infoHash: 'bittorrent://example-link-infoHash',
            magnet: 'magnet:?xt:urn:btih:example-link&xs=urn:btpk:example-link'
          }
        },
        protocols: {
          http: true,
          hyper: false,
          ipfs: true,
          bittorrent: true
        }
      }

      expect(@site.call(json).errors.empty?).to(eql(true))
    end

    it 'can load a site config without public field (optional)' do
      json = {
        id: Nanoid.generate,
        domain: 'sutty.ml',
        links: {
          http: {
            enabled: true,
            link: 'http://sutty.ml/'
          },
          hyper: {
            enabled: true,
            link: 'hyper://sutty.ml/',
            gateway: 'https://distributed.press/hyper/sutty.ml/',
            raw: 'hyper://465kqqkfg9gon1wf5c373jx6fk46/'
          },
          ipfs: {
            enabled: true,
            link: 'ipns://sutty.ml/',
            gateway: 'https://distributed.press/ipns/sutty.ml/',
            cid: '465kqqkfg9gon1wf5c373jx6fk46',
            pubKey: '465kqqkfg9gon1wf5c373jx6fk46'
          }
        },
        protocols: {
          http: true,
          hyper: false,
          ipfs: true
        }
      }

      expect(@site.call(json).errors.empty?).to(eql(true))
    end

    it 'cannot load a site without domain' do
      json = {
        id: Nanoid.generate,
        public: true,
        links: {
          http: {
            enabled: true,
            link: 'http://sutty.ml/'
          },
          hyper: {
            enabled: true,
            link: 'hyper://sutty.ml/',
            gateway: 'https://distributed.press/hyper/sutty.ml/',
            raw: 'hyper://465kqqkfg9gon1wf5c373jx6fk46/'
          },
          ipfs: {
            enabled: true,
            link: 'ipns://sutty.ml/',
            gateway: 'https://distributed.press/ipns/sutty.ml/',
            cid: '465kqqkfg9gon1wf5c373jx6fk46',
            pubKey: '465kqqkfg9gon1wf5c373jx6fk46'
          }
        },
        protocols: {
          http: true,
          hyper: false,
          ipfs: true
        }
      }

      expect(@site.call(json).errors.empty?).to(eql(false))
    end

    it 'cannot load a site config without links' do
      json = {
        id: Nanoid.generate,
        domain: 'sutty.ml',
        public: false,
        protocols: {
          http: true,
          hyper: false,
          ipfs: true,
          bittorrent: true
        }
      }

      expect(@site.call(json).errors.empty?).to(eql(false))
    end

    it 'cannot load a site config without protocols' do
      json = {
        id: Nanoid.generate,
        domain: 'sutty.ml',
        public: false,
        links: {
          http: {
            enabled: true,
            link: 'http://sutty.ml/'
          },
          hyper: {
            enabled: true,
            link: 'hyper://sutty.ml/',
            gateway: 'https://distributed.press/hyper/sutty.ml/',
            raw: 'hyper://465kqqkfg9gon1wf5c373jx6fk46/'
          },
          ipfs: {
            enabled: true,
            link: 'ipns://sutty.ml/',
            gateway: 'https://distributed.press/ipns/sutty.ml/',
            cid: '465kqqkfg9gon1wf5c373jx6fk46',
            pubKey: '465kqqkfg9gon1wf5c373jx6fk46'
          },
          bittorrent: {
            enabled: true,
            link: 'bittorrent://sutty.ml/',
            gateway: 'https://distributed.press/bittorrent/sutty.ml/',
            dnslink: '/bt/example-raw',
            pubKey: '465kqqkfg9gon1wf5c373jx6fk46',
            infoHash: 'bittorrent://example-link-infoHash',
            magnet: 'magnet:?xt:urn:btih:example-link&xs=urn:btpk:example-link'
          }
        }
      }

      expect(@site.call(json).errors.empty?).to(eql(false))
    end

    it 'can load a site config without bittorrent protocol' do
      json = {
        id: Nanoid.generate,
        domain: 'sutty.ml',
        public: false,
        links: {
          http: {
            enabled: true,
            link: 'http://sutty.ml/'
          },
          hyper: {
            enabled: true,
            link: 'hyper://sutty.ml/',
            gateway: 'https://distributed.press/hyper/sutty.ml/',
            raw: 'hyper://465kqqkfg9gon1wf5c373jx6fk46/'
          },
          ipfs: {
            enabled: true,
            link: 'ipns://sutty.ml/',
            gateway: 'https://distributed.press/ipns/sutty.ml/',
            cid: '465kqqkfg9gon1wf5c373jx6fk46',
            pubKey: '465kqqkfg9gon1wf5c373jx6fk46'
          }
        },
        protocols: {
          http: true,
          hyper: false,
          ipfs: true
        }
      }

      expect(@site.call(json).errors.empty?).to(eql(true))
    end
  end
end
