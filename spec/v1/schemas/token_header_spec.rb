# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/token_header'

RSpec.describe DistributedPress::V1::Schemas::TokenHeader do
  before do
    @token_header = DistributedPress::V1::Schemas::TokenHeader.new
  end

  describe '#call' do
    it 'can load a token header' do
      json = {
        alg: 'RS256',
        typ: 'JWT'
      }

      expect(@token_header.call(json).success?).to(eql(true))
    end
  end
end
