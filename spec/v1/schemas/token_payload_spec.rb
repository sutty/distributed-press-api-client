# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/token_payload'

RSpec.describe DistributedPress::V1::Schemas::TokenPayload do
  before do
    @token_payload = DistributedPress::V1::Schemas::TokenPayload.new
  end

  describe '#call' do
    it 'can load a token payload factory' do
      token_payload = build :token_payload

      expect(token_payload.success?).to(eql(true))
    end

    it 'can load a raw token payload' do
      caps = DistributedPress::V1::Schemas::TokenPayload::CAPABILITIES
      json = {
        issuedTo: 'system',
        tokenId: Nanoid.generate,
        iat: Time.now.to_i * 1000,
        expires: -1,
        capabilities: caps.sample(rand(1..caps.size))
      }

      expect(@token_payload.call(json).success?).to(eql(true))
    end
  end
end
