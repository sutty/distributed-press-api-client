# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/schemas/update_site'

RSpec.describe DistributedPress::V1::Schemas::UpdateSite do
  before do
    @update_site = DistributedPress::V1::Schemas::UpdateSite.new
  end

  describe '#call' do
    it 'cannot load a site config' do
      json = {
        id: Nanoid.generate,
        public: false,
        domain: 'sutty.ml'
      }

      expect(@update_site.call(json).errors.empty?).to(eql(false))
    end

    it 'can load a site config with protocols' do
      json = {
        id: Nanoid.generate,
        domain: 'sutty.ml',
        public: false,
        protocols: {
          http: true,
          hyper: false,
          ipfs: true
        }
      }

      expect(@update_site.call(json).errors.empty?).to(eql(true))
    end

    it 'can load a site without public field' do
      json = {
        id: Nanoid.generate,
        domain: 'sutty.ml',
        protocols: {
          http: true,
          hyper: false,
          ipfs: true
        }
      }

      expect(@update_site.call(json).errors.empty?).to(eql(true))
    end
  end
end
