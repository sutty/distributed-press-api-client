# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/blocklist'

klass = DistributedPress::V1::Social::Blocklist

RSpec.describe klass do
  before do
    rsa_pem = File.read('spec/fixtures/rsa.pem')
    @client = DistributedPress::V1::Social::Client.new(public_key_url: 'this public key is not in use',
                                                       private_key_pem: rsa_pem)
    @client.class.cache_store.clear
    @blocklist = klass.new(client: @client)
  end

  describe '#new' do
    it 'can be initialized with a client' do
      expect(@blocklist.client).to(eql(@client))
    end
  end

  describe '#post' do
    it 'signs requests' do
      stub_request(:post, 'https://social.distributed.press/v1/blocklist')
        .with(headers: { Accept: 'text/plain' })
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@blocklist.post(list: []).parsed_response).to(eql(%w[OK]))
    end
  end

  describe '#get' do
    it 'signs requests' do
      stub_request(:get, 'https://social.distributed.press/v1/blocklist')
        .with(headers: { Accept: 'text/plain' })
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@blocklist.get.parsed_response).to(eql(%w[OK]))
    end

    it 'returns an array' do
      stub_request(:get, 'https://social.distributed.press/v1/blocklist')
        .with(headers: { Accept: 'text/plain' })
        .to_return do |_request|
          {
            status: 200,
            body: "OK\n\nOK\n",
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@blocklist.get.parsed_response).to(eql(%w[OK OK]))
    end
  end

  describe '#delete' do
    it 'sends blocklists' do
      stub_request(:delete, 'https://social.distributed.press/v1/blocklist')
        .with(headers: { Accept: 'text/plain' })
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@blocklist.delete(list: []).parsed_response).to(eql(%w[OK]))
    end
  end
end
