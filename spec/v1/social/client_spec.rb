# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/client'

klass = DistributedPress::V1::Social::Client

RSpec.describe klass do
  before do
    @client = klass.new(public_key_url: nil)
    @client.class.cache_store.clear
  end

  def hash_from_signature(signature)
    signature.split(',').map do |pair|
      pair.split('=', 2).map do |value|
        value.gsub(/\A"/, '').gsub(/"\z/, '')
      end
    end.to_h
  end

  def decode_signature(signature)
    Base64.decode64(signature)
  end

  def recover_signature_content(headers, signed_hash, method)
    signed_hash['headers'].split(' ').map do |signed_header_name|
      if signed_header_name == '(request-target)'
        "(request-target): #{method} /outbox/"
      else
        "#{signed_header_name}: #{headers[signed_header_name.capitalize]}"
      end
    end.join("\n")
  end

  def verify(headers, method = 'post')
    signed_hash = hash_from_signature(headers['Signature'])
    signature = decode_signature(signed_hash['signature'])
    comparison_string = recover_signature_content(headers, signed_hash, method)

    @client.public_key.verify(OpenSSL::Digest.new('SHA256'), signature, comparison_string)
  end

  describe '#new' do
    it 'can be initialized with a key size' do
      client = klass.new(public_key_url: nil, key_size: 1024)

      expect(client.private_key.class).to(eql(OpenSSL::PKey::RSA))
    end

    it 'can be initialized with a private key' do
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      client = klass.new(public_key_url: nil, private_key_pem: rsa_pem)

      expect(client.private_key.class).to(eql(OpenSSL::PKey::RSA))
      expect(client.private_key.export).to(eql(rsa_pem))
    end

    it 'includes host on default headers' do
      expect(@client.send(:default_headers)['Host']).to(eql('social.distributed.press'))
    end

    it 'can find the host of distributed press' do
      expect(@client.host).to(eql('social.distributed.press'))
    end

    it 'can work with other instances' do
      client = klass.new(public_key_url: nil, url: 'https://instance2.social.distributed.press')

      expect(client.host).to(eql('instance2.social.distributed.press'))
    end
  end

  describe '#checksum_body!' do
    it 'can checksum contents of body' do
      checksum = 'SHA-256=RBNvo1WzZ4oRRq0W9+hknpT7T8If536DEMBg9hyq/4o='
      body = '{}'
      headers = {}

      expect(@client.send(:checksum_body!, body, headers)).to(eql(nil))
      expect(headers['Digest']).to(eql(checksum))
    end
  end

  describe '#post' do
    it 'signs requests' do
      stub_request(:post, 'https://social.distributed.press/outbox/')
        .to_return do |request|
          verification = verify request.headers

          {
            status: (verification ? 200 : 403),
            body: (verification ? 'OK' : 'ERROR')
          }
        end
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      @client = klass.new(public_key_url: 'this public key is not in use', private_key_pem: rsa_pem)

      expect(@client.post(body: {}, endpoint: '/outbox/').body).to(eql('OK'))
    end
  end

  describe '#get' do
    it 'signs requests' do
      stub_request(:get, 'https://social.distributed.press/outbox/')
        .to_return do |request|
          verification = verify request.headers, 'get'

          {
            status: (verification ? 200 : 403),
            body: (verification ? 'OK' : 'ERROR')
          }
        end
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      @client = klass.new(public_key_url: 'this public key is not in use', private_key_pem: rsa_pem)

      expect(@client.get(endpoint: '/outbox/').body).to(eql('OK'))
    end

    it 'requests brotli compression' do
      stub_request(:get, 'https://social.distributed.press/outbox/')
        .to_return do |_request|
          {
            status: 200,
            body: File.open('spec/fixtures/ok.br'),
            headers: {
              'Content-Encoding': 'br'
            }
          }
        end
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      @client = klass.new(public_key_url: 'this public key is not in use', private_key_pem: rsa_pem)

      expect(@client.get(endpoint: '/outbox/').body).to(eql('OK'))
    end

    it 'prevents gzip bombs' do
      skip 'webmock does not support gzip decoding'

      stub_request(:get, 'https://social.distributed.press/outbox/')
        .to_return do |_request|
          {
            status: 200,
            body: File.open('spec/fixtures/bomb.gz'),
            headers: {
              'Content-Encoding': 'gzip'
            }
          }
        end
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      @client = klass.new(public_key_url: 'this public key is not in use', private_key_pem: rsa_pem)

      expect do
        @client.get(endpoint: '/outbox/')
      end.to(raise_error(klass::ContentTooLargeError))
    end

    it 'prevents brotli bombs' do
      stub_request(:get, 'https://social.distributed.press/outbox/')
        .to_return do |_request|
          {
            status: 200,
            body: File.open('spec/fixtures/bomb.br'),
            headers: {
              'Content-Encoding': 'br'
            }
          }
        end
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      @client = klass.new(public_key_url: 'this public key is not in use', private_key_pem: rsa_pem)

      expect do
        @client.get(endpoint: '/outbox/')
      end.to(raise_error(klass::ContentTooLargeError))
    end

    it 'caches' do
      body = 'OK'

      stub_request(:get, 'https://social.distributed.press/outbox/')
        .to_return do |request|
          body = 'INVALID SIGNATURE' unless verify(request.headers, 'get')

          {
            status: 200,
            body: body,
            headers: {
              'Cache-Control': 'no-cache'
            }
          }
        end

      stub_request(:head, 'https://social.distributed.press/outbox/')
        .to_return do |request|
          body = 'INVALID SIGNATURE' unless verify(request.headers, 'head')

          { status: 304 }
        end
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      @client = klass.new(public_key_url: 'this public key is not in use', private_key_pem: rsa_pem)

      first = @client.get(endpoint: '/outbox/')
      last = @client.get(endpoint: '/outbox/')

      expect(first.revalidate?).to(eql(true))
      expect(first.parsed_response).to(eql('OK'))
      expect(last.parsed_response).to(eql('OK'))
    end

    it 'revalidates caches' do
      body = 'OK'

      stub_request(:get, 'https://social.distributed.press/outbox/')
        .to_return do |request|
          body = 'INVALID SIGNATURE' unless verify(request.headers, 'get')

          {
            status: 200,
            body: body,
            headers: {
              'Cache-Control': 'no-cache'
            }
          }
        end

      stub_request(:head, 'https://social.distributed.press/outbox/')
        .to_return do |request|
          body = 'NOTCACHED'
          body = 'INVALID SIGNATURE' unless verify(request.headers, 'head')

          { status: 200 }
        end
      rsa_pem = File.read('spec/fixtures/rsa.pem')
      @client = klass.new(public_key_url: 'this public key is not in use', private_key_pem: rsa_pem)

      first = @client.get(endpoint: '/outbox/')
      last = @client.get(endpoint: '/outbox/')

      expect(first.revalidate?).to(eql(true))
      expect(first.parsed_response).to(eql('OK'))
      expect(last.parsed_response).to(eql('NOTCACHED'))
    end
  end
end
