# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/collection'
require 'distributed_press/v1/social/client'
require 'distributed_press/v1/social/dereferencer'

klass = DistributedPress::V1::Social::Collection

RSpec.describe klass do
  before do
    @key = SecureRandom.hex
    @val = SecureRandom.hex
    @client = DistributedPress::V1::Social::Client.new(public_key_url: nil)
    @dereferencer = DistributedPress::V1::Social::Dereferencer.new(client: @client)

    @items = rand(100).times.map do
      "https://instance-#{SecureRandom.hex}.com"
    end

    @ref = klass.new(object: { 'type' => 'Collection', 'totalItems' => @items.size, 'items' => @items }, dereferencer: @dereferencer)
  end

  describe '#each' do
    it 'iterates over items' do
      @ref.each_with_index do |item, i|
        expect(item).to(eql(@items[i]))
      end

      expect(@ref.each.to_a.size).to(eql(@ref.object['totalItems']))
    end

    it 'iterates the first page' do
      skip
      @ref.object['first'] = { 'type' => 'CollectionPage', 'items' => @ref.object.delete('items') }

      @ref.each_with_index do |item, i|
        expect(item).to(eql(@items[i]))
      end

      expect(@ref.each.to_a.size).to(eql(@ref.object['totalItems']))

      @ref.each do |item|
        expect(@items.include?(item)).to(eql(true))
      end
    end

    it 'iterates over all pages' do
      skip
      @ref.object.delete('items')
      pages = []

      @items.each_slice(@items.size / (rand(10) + 2)) do |items|
        pages << { 'type' => 'CollectionPage', 'items' => items }
      end

      pages.reduce do |prev, cur|
        prev['next'] = cur

        cur
      end

      @ref.object['first'] = pages.first

      @ref.each_with_index do |item, i|
        expect(item).to(eql(@items[i]))
      end

      expect(@ref.each.to_a.size).to(eql(@ref.object['totalItems']))
    end

    it 'stops at the right amount of items' do
      total = rand(@items.size - 1)
      @ref = klass.new(object: { 'type' => 'Collection', 'totalItems' => total, 'items' => @items }, dereferencer: @dereferencer)

      expect(@ref.each.to_a.size).to(eql(@ref.object['totalItems']))
      expect(total).to(eql(@ref.object['totalItems']))
    end
  end
end
