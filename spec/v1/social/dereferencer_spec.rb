# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/reference'
require 'distributed_press/v1/social/client'
require 'distributed_press/v1/social/dereferencer'

klass = DistributedPress::V1::Social::Dereferencer

RSpec.describe klass do
  before do
    @uri = "https://social.distributed.press/#{SecureRandom.hex}/"
    @key = DistributedPress::V1::Social::ReferencedObject::REFERENTIABLE_ATTRIBUTES.sample
    @val = "https://another.social.instance/#{SecureRandom.hex}/"

    stub_request(:get, @uri)
      .to_return do |_request|
        {
          status: 200,
          body: { @key => @val }.to_json,
          headers: {
            'Content-Type': 'application/ld+json'
          }
        }
      end

    stub_request(:head, @uri)
      .to_return do |_request|
        { status: 200 }
      end

    stub_request(:get, @val)
      .to_return do |_request|
        {
          status: 200,
          body: { 'id' => @val }.to_json,
          headers: {
            'Content-Type': 'application/ld+json'
          }
        }
      end

    @client = DistributedPress::V1::Social::Client.new(public_key_url: nil)
    @client.class.cache_store.clear
    @dereferencer = DistributedPress::V1::Social::Dereferencer.new(client: @client)
  end

  describe '#new' do
    it 'initializes' do
      expect(@dereferencer.client).to(eql(@client))
    end
  end

  describe '#get' do
    it 'references' do
      expect(@dereferencer.get(uri: @uri)[@key].class).to(eql(DistributedPress::V1::Social::Reference))
    end

    it 'dereferences' do
      expect(@dereferencer.get(uri: @uri)[@key]['id']).to(eql(@val))
      expect(@dereferencer.get(uri: @uri).dig(@key, 'id')).to(eql(@val))
    end

    it 'canonicalizes' do
      html = "#{@uri}index.html"
      json = "#{@uri}index.jsonld"

      stub_request(:get, html)
        .to_return do |_|
          {
            status: 200,
            body: "<link rel='alternate' type='application/activity+json' href='#{json}' />",
            headers: {
              'Content-Type': 'text/html'
            }
          }
        end

      stub_request(:get, json)
        .to_return do |_|
          {
            status: 200,
            body: { 'id' => json }.to_json,
            headers: {
              'Content-Type': 'application/activity+json'
            }
          }
        end

      expect(@dereferencer.get(uri: html).parsed_response.class).to(eql(DistributedPress::V1::Social::ReferencedObject))
    end

    it 'canonicalizes ld+json' do
      html = "#{@uri}index.html"
      json = "#{@uri}index.jsonld"

      stub_request(:get, html)
        .to_return do |_|
          {
            status: 200,
            body: "<link rel='alternate' type='application/ld+json; profile=...' href='#{json}' />",
            headers: {
              'Content-Type': 'text/html'
            }
          }
        end

      stub_request(:get, json)
        .to_return do |_|
          {
            status: 200,
            body: { 'id' => json }.to_json,
            headers: {
              'Content-Type': 'application/activity+json'
            }
          }
        end

      expect(@dereferencer.get(uri: html).parsed_response.class).to(eql(DistributedPress::V1::Social::ReferencedObject))
    end

    it 'does not canonicalize the public address' do
      html = "#{@uri}index.html"
      json = "#{@uri}index.jsonld"

      stub_request(:get, html)
        .to_return do |_|
          {
            status: 200,
            body: "<link rel='alternate' type='application/activity+json' href='#{json}' />",
            headers: {
              'Content-Type': 'text/html'
            }
          }
        end

      stub_request(:get, json)
        .to_return do |_|
          {
            status: 200,
            body: { 'id' => json, 'to' => ['https://www.w3.org/ns/activitystreams#Public'] }.to_json,
            headers: {
              'Content-Type': 'application/activity+json'
            }
          }
        end

      expect(@dereferencer.get(uri: html).parsed_response['to'].first).to(eql(DistributedPress::V1::Social::ReferencedObject::PUBLIC))
    end
  end

  describe '#uris' do
    it 'parses uris' do
      parsed_uri = Addressable::URI.parse(@uri)

      expect(@dereferencer.uris(@uri).class).to(eql(Addressable::URI))
      expect(@dereferencer.uris(@uri)).to(eql(parsed_uri))
    end

    it 'accepts parsed uris' do
      parsed_uri = Addressable::URI.parse(@uri)

      expect(@dereferencer.uris(parsed_uri)).to(eql(parsed_uri))
    end
  end

  describe '#clients' do
    it 'instantiates clients per uri' do
      parsed_uri = Addressable::URI.parse(@uri)

      expect(@dereferencer.clients(parsed_uri).class).to(eql(DistributedPress::V1::Social::Client))
      expect(@dereferencer.clients(parsed_uri).url).to(eql(parsed_uri.origin))
    end
  end

  describe '#references' do
    it 'instantiates references per uri' do
      expect(@dereferencer.references(@uri).class).to(eql(DistributedPress::V1::Social::Reference))
    end
  end
end
