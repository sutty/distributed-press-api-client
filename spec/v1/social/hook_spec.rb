# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/hook'

klass = DistributedPress::V1::Social::Hook

RSpec.describe klass do
  before do
    rsa_pem = File.read('spec/fixtures/rsa.pem')
    @client = DistributedPress::V1::Social::Client.new(public_key_url: 'this public key is not in use',
                                                       private_key_pem: rsa_pem)
    @hook = klass.new(client: @client, actor: '@sutty@sutty.nl')
    @event = klass::EVENTS.sample
    @webhook = build :webhook
    @client.class.cache_store.clear
  end

  describe '#new' do
    it 'can be initialized with a client' do
      expect(@hook.client).to(eql(@client))
    end
  end

  describe '#put' do
    it 'creates hooks' do
      stub_request(:put, "https://social.distributed.press/v1/@sutty@sutty.nl/hooks/#{@event}")
        .to_return do |_request|
          {
            status: 200,
            body: 'Hook set successfully',
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@hook.put(event: @event, hook: @webhook).ok?).to(eql(true))
    end

    it 'validates hooks' do
      expect do
        @hook.put(event: SecureRandom.hex, hook: @webhook).ok?
      end.to(raise_error(klass::EventNotValidError))
    end
  end

  describe '#get' do
    it 'gets an event' do
      stub_request(:get, "https://social.distributed.press/v1/@sutty@sutty.nl/hooks/#{@event}")
        .to_return do |_request|
          {
            status: 200,
            body: @webhook.to_h.to_json,
            headers: {
              'Content-Type': 'application/json'
            }
          }
        end
      stub_request(:head, "https://social.distributed.press/v1/@sutty@sutty.nl/hooks/#{@event}")
        .to_return do |_request|
          { status: 304 }
        end

      expect(@hook.get(event: @event).parsed_response.errors.empty?).to(eql(true))
      expect(@hook.get(event: @event).parsed_response.errors.empty?).to(eql(true))
    end

    it 'validates hooks' do
      stub_request(:get, "https://social.distributed.press/v1/@sutty@sutty.nl/hooks/#{@event}")
        .to_return do |_request|
          {
            status: 200,
            body: '{}',
            headers: {
              'Content-Type': 'application/json'
            }
          }
        end

      expect(@hook.get(event: @event).parsed_response.errors.empty?).to(eql(false))
    end
  end

  describe '#delete' do
    it 'removes hooks' do
      stub_request(:delete, "https://social.distributed.press/v1/@sutty@sutty.nl/hooks/#{@event}")
        .to_return do |_request|
          {
            status: 200,
            body: 'Hook deleted successfully',
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@hook.delete(event: @event).parsed_response).to(eql('Hook deleted successfully'))
    end
  end
end
