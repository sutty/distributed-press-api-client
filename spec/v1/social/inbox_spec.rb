# frozen_string_literal: true

require 'base64'
require 'spec_helper'
require 'distributed_press/v1/social/inbox'

klass = DistributedPress::V1::Social::Inbox

RSpec.describe klass do
  before do
    rsa_pem = File.read('spec/fixtures/rsa.pem')
    @client = DistributedPress::V1::Social::Client.new(public_key_url: 'this public key is not in use',
                                                       private_key_pem: rsa_pem)
    @inbox = klass.new(client: @client, actor: '@sutty@sutty.nl')
    @client.class.cache_store.clear
  end

  describe '#new' do
    it 'can be initialized with a client' do
      expect(@inbox.client).to(eql(@client))
    end
  end

  describe '#post' do
    it 'signs requests' do
      stub_request(:post, 'https://social.distributed.press/v1/@sutty@sutty.nl/inbox')
        .to_return do |_request|
          {
            status: 200,
            body: { message: 'ok' }.to_json,
            headers: {
              'Content-Type': 'application/json'
            }
          }
        end

      expect(@inbox.post(activity: {}).parsed_response['message']).to(eql('ok'))
    end
  end

  describe '#get' do
    it 'gets an inbox' do
      stub_request(:get, 'https://social.distributed.press/v1/@sutty@sutty.nl/inbox')
        .to_return do |_request|
          {
            status: 200,
            body: {
              '@context' => 'https://www.w3.org/ns/activitystreams',
              'type' => 'OrderedCollection',
              'id' => '/v1/@sutty@sutty.nl/inbox',
              'orderedItems' => []
            }.to_json,
            headers: {
              'Content-Type': @client.class::ACCEPT.sample
            }
          }
        end

      expect(@inbox.get.parsed_response['type']).to(eql('OrderedCollection'))
    end
  end

  describe '#accept' do
    it 'sends inboxs' do
      base64 = Base64.encode64('https://social.distributed.press/v1/@sutty@sutty.nl/inbox').delete("\n")

      stub_request(:post, "https://social.distributed.press/v1/@sutty@sutty.nl/inbox/#{base64}")
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@inbox.accept(id: 'https://social.distributed.press/v1/@sutty@sutty.nl/inbox').parsed_response).to(eql('OK'))
    end
  end

  describe '#reject' do
    it 'sends inboxs' do
      base64 = Base64.encode64('https://social.distributed.press/v1/@sutty@sutty.nl/inbox').delete("\n")

      stub_request(:delete, "https://social.distributed.press/v1/@sutty@sutty.nl/inbox/#{base64}")
        .to_return do |_request|
          {
            status: 200,
            body: 'OK',
            headers: {
              'Content-Type': 'text/plain'
            }
          }
        end

      expect(@inbox.reject(id: 'https://social.distributed.press/v1/@sutty@sutty.nl/inbox').parsed_response).to(eql('OK'))
    end
  end
end
