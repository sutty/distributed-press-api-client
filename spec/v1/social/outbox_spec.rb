# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/outbox'

klass = DistributedPress::V1::Social::Outbox

RSpec.describe klass do
  before do
    rsa_pem = File.read('spec/fixtures/rsa.pem')
    @client = DistributedPress::V1::Social::Client.new(public_key_url: 'this public key is not in use',
                                                       private_key_pem: rsa_pem)
    @outbox = klass.new(client: @client, actor: '@sutty@sutty.nl')
    @client.class.cache_store.clear
  end

  describe '#new' do
    it 'can be initialized with a client' do
      expect(@outbox.client).to(eql(@client))
    end
  end

  describe '#post' do
    it 'signs requests' do
      stub_request(:post, 'https://social.distributed.press/v1/@sutty@sutty.nl/outbox')
        .to_return do |_request|
          {
            status: 200,
            body: { message: 'ok' }.to_json,
            headers: {
              'Content-Type': 'application/json'
            }
          }
        end

      expect(@outbox.post(activity: {}).parsed_response['message']).to(eql('ok'))
    end
  end

  describe '#get' do
    it 'gets an activity from the outbox' do
      stub_request(:get, 'https://social.distributed.press/v1/@sutty@sutty.nl/outbox/ID')
        .to_return do |_request|
          {
            status: 200,
            body: {
              '@context' => 'https://www.w3.org/ns/activitystreams',
              'type' => 'OrderedCollection',
              'id' => '/v1/@sutty@sutty.nl/outbox',
              'orderedItems' => []
            }.to_json,
            headers: {
              'Content-Type': @client.class::ACCEPT.sample
            }
          }
        end

      expect(@outbox.get(id: 'ID').parsed_response['type']).to(eql('OrderedCollection'))
    end
  end
end
