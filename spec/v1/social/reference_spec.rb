# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/reference'
require 'distributed_press/v1/social/client'
require 'distributed_press/v1/social/dereferencer'

klass = DistributedPress::V1::Social::Reference

class Hash
  def to_referenced_object
    DistributedPress::V1::Social::ReferencedObject.new(dereferencer: nil, object: self, referenced: self)
  end
end

RSpec.describe klass do
  before do
    @key = SecureRandom.hex
    @val = SecureRandom.hex
    @ref = klass.new(uri: nil, dereferencer: nil)

    @ref.instance_variable_set :@object, { @key => @val }
  end

  describe '#new' do
    it 'initializes' do
      uri = SecureRandom.hex
      dereferencer = SecureRandom.hex

      @ref = klass.new(uri: uri, dereferencer: dereferencer)

      expect(@ref.uri).to(eql(uri))
      expect(@ref.dereferencer).to(eql(dereferencer))
    end
  end

  describe '#object' do
    it 'fetches remote objects' do
      uri = "https://social.distributed.press/#{SecureRandom.hex}/"

      stub_request(:get, uri)
        .to_return do |_request|
          {
            status: 200,
            body: { @key => @val }.to_json,
            headers: {
              'Content-Type': 'application/ld+json'
            }
          }
        end

      client = DistributedPress::V1::Social::Client.new(public_key_url: nil)
      client.class.cache_store.clear
      dereferencer = DistributedPress::V1::Social::Dereferencer.new(client: client)
      @ref = klass.new(uri: uri, dereferencer: dereferencer)

      expect(@ref.object.to_h).to(eql({ @key => @val }))
    end
  end

  describe '#[]' do
    it 'delegates to the object' do
      expect(@ref[@key]).to(eql(@val))
    end
  end

  describe '#to_json' do
    it 'delegates to the object' do
      expect(@ref.to_json).to(eql({ @key => @val }.to_json))
    end
  end

  describe '#dig' do
    it 'delegates to the object' do
      @ref.instance_variable_set :@object, { @key => { @val => @val } }

      expect(@ref.dig(@key, @val)).to(eql(@val))
    end
  end

  describe '#public?' do
    it 'can be public' do
      @ref.instance_variable_set :@object, { 'to' => [DistributedPress::V1::Social::ReferencedObject::PUBLIC] }.to_referenced_object

      expect(@ref.public?).to(eql(true))
    end

    it 'is public when no audience is set' do
      @ref.instance_variable_set :@object, {}.to_referenced_object

      expect(@ref.public?).to(eql(true))
    end

    it 'can have an audience' do
      @ref.instance_variable_set :@object, { 'audience' => DistributedPress::V1::Social::ReferencedObject::PUBLIC }.to_referenced_object

      expect(@ref.public?).to(eql(true))
    end

    it 'cannot be on cc' do
      @ref.instance_variable_set :@object, { 'to' => ['@distributed@press'], 'cc' => [DistributedPress::V1::Social::ReferencedObject::PUBLIC] }.to_referenced_object

      expect(@ref.public?).to(eql(false))
    end

    it 'can be private' do
      @ref.instance_variable_set :@object,
                                 { 'cc' => [SecureRandom.hex], 'to' => [SecureRandom.hex],
                                   'audience' => SecureRandom.hex }.to_referenced_object

      expect(@ref.public?).to(eql(false))
    end
  end

  describe '#unlisted?' do
    it 'can be unlisted' do
      @ref.instance_variable_set :@object, { 'to' => [DistributedPress::V1::Social::ReferencedObject::PUBLIC] }.to_referenced_object

      expect(@ref.unlisted?).to(eql(false))
    end

    it 'can have an audience' do
      @ref.instance_variable_set :@object, { 'audience' => DistributedPress::V1::Social::ReferencedObject::PUBLIC }.to_referenced_object

      expect(@ref.unlisted?).to(eql(false))
    end

    it 'cannot be on cc' do
      @ref.instance_variable_set :@object, { 'cc' => [DistributedPress::V1::Social::ReferencedObject::PUBLIC] }.to_referenced_object

      expect(@ref.unlisted?).to(eql(true))
    end

    it 'can be private' do
      @ref.instance_variable_set :@object,
                                 { 'cc' => [SecureRandom.hex], 'to' => [SecureRandom.hex],
                                   'audience' => SecureRandom.hex }.to_referenced_object

      expect(@ref.unlisted?).to(eql(false))
    end
  end

  describe '#private?' do
    it 'can be public' do
      @ref.instance_variable_set :@object, { 'to' => [DistributedPress::V1::Social::ReferencedObject::PUBLIC] }.to_referenced_object

      expect(@ref.private?).to(eql(false))
    end

    it 'can have an audience' do
      @ref.instance_variable_set :@object, { 'audience' => DistributedPress::V1::Social::ReferencedObject::PUBLIC }.to_referenced_object

      expect(@ref.private?).to(eql(false))
    end

    it 'cannot be on cc' do
      @ref.instance_variable_set :@object, { 'cc' => [DistributedPress::V1::Social::ReferencedObject::PUBLIC] }.to_referenced_object

      expect(@ref.private?).to(eql(false))
    end

    it 'can be private' do
      @ref.instance_variable_set :@object,
                                 { 'cc' => [SecureRandom.hex], 'to' => [SecureRandom.hex],
                                   'audience' => SecureRandom.hex }.to_referenced_object

      expect(@ref.private?).to(eql(true))
    end
  end
end
