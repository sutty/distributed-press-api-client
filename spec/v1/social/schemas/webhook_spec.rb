# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/schemas/webhook'

RSpec.describe DistributedPress::V1::Social::Schemas::Webhook do
  before do
    @webhook = DistributedPress::V1::Social::Schemas::Webhook.new
  end

  describe '#call' do
    it 'can load a webhook' do
      json = {
        url: 'https://panel.sutty.nl',
        method: DistributedPress::V1::Social::Schemas::Webhook::METHODS.sample,
        headers: {}
      }

      expect(@webhook.call(json).errors.empty?).to(eql(true))
    end

    it 'cannot load a webhook without method' do
      json = {
        url: 'https://panel.sutty.nl',
        headers: {}
      }

      expect(@webhook.call(json).errors.empty?).to(eql(false))
    end

    it 'cannot load a webhook without url' do
      json = {
        method: DistributedPress::V1::Social::Schemas::Webhook::METHODS.sample,
        headers: {}
      }

      expect(@webhook.call(json).errors.empty?).to(eql(false))
    end

    it 'can have any number of headers' do
      json = {
        url: 'https://panel.sutty.nl',
        method: DistributedPress::V1::Social::Schemas::Webhook::METHODS.sample,
        headers: (
          rand(1..100).times.map do
            [SecureRandom.hex, SecureRandom.hex]
          end
        ).to_h
      }

      expect(@webhook.call(json).errors.empty?).to(eql(true))
    end
  end
end
