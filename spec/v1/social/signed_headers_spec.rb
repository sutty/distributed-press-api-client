# frozen_string_literal: true

require 'spec_helper'
require 'distributed_press/v1/social/client'
require 'distributed_press/v1/social/signed_headers'

klass = DistributedPress::V1::Social::SignedHeaders

RSpec.describe klass do
  before do
    @client = DistributedPress::V1::Social::Client.new(public_key_url: 'something')
    @client.class.cache_store.clear
    @headers = @client.send(:default_headers)
    @headers.request = HTTParty::Request.new(Net::HTTP::Post, '/outbox/')
  end

  def hash_from_signature(signature)
    signature.split(',').map do |pair|
      pair.split('=', 2).map do |value|
        value.gsub(/\A"/, '').gsub(/"\z/, '')
      end
    end.to_h
  end

  def decode_signature(signature)
    Base64.decode64(signature)
  end

  def recover_signature_content(headers, signed_hash, method)
    signed_hash['headers'].split(' ').map do |signed_header_name|
      if signed_header_name == '(request-target)'
        "(request-target): #{method} /outbox/"
      else
        "#{signed_header_name}: #{headers[signed_header_name.capitalize]}"
      end
    end.join("\n")
  end

  def verify(headers, method = 'post')
    signed_hash = hash_from_signature(headers['Signature'])
    signature = decode_signature(signed_hash['signature'])
    comparison_string = recover_signature_content(headers, signed_hash, method)

    @client.public_key.verify(OpenSSL::Digest.new('SHA256'), signature, comparison_string)
  end

  describe '#signable_headers' do
    it 'returns a list' do
      @headers = klass[
        '(request-target)' => '',
        'Host' => '',
        'Date' => '',
        'Digest' => ''
      ]

      expect(@headers.send(:signable_headers)).to(eql('(request-target) host date digest'))
    end

    it 'can be customized' do
      klass::SIGNABLE_HEADERS << 'Custom'
      @headers = klass[
        '(request-target)' => '',
        'Host' => '',
        'Date' => '',
        'Digest' => '',
        'Custom' => ''
      ]

      expect(@headers.send(:signable_headers)).to(eql('(request-target) host date digest custom'))
    end

    it 'ignores unset headers' do
      klass::SIGNABLE_HEADERS << 'Custom'
      @headers = klass[
        '(request-target)' => '',
        'Host' => '',
        'Date' => '',
        'Digest' => ''
      ]

      expect(@headers.send(:signable_headers)).to(eql('(request-target) host date digest'))
    end
  end

  describe '#signature_content' do
    it 'returns a content' do
      body = '{}'

      @client.send(:checksum_body!, body, @headers)
      @headers.send(:request_target!)

      content = <<~CONTENT
        (request-target): post /outbox/
        host: social.distributed.press
        date: #{@headers['Date']}
        digest: SHA-256=RBNvo1WzZ4oRRq0W9+hknpT7T8If536DEMBg9hyq/4o=
      CONTENT

      expect(@headers.send(:signature_content)).to(eql(content.chomp))
    end

    # This is basically the same test as before with different order on
    # headers.
    it 'follows headers order' do
      body = '{}'

      @headers.send(:request_target!)
      @client.send(:checksum_body!, body, @headers)

      content = <<~CONTENT
        (request-target): post /outbox/
        host: social.distributed.press
        date: #{@headers['Date']}
        digest: SHA-256=RBNvo1WzZ4oRRq0W9+hknpT7T8If536DEMBg9hyq/4o=
      CONTENT

      expect(@headers.send(:signature_content)).to(eql(content.chomp))
    end
  end
end
