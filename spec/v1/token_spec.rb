# frozen_string_literal: true

require 'securerandom'
require 'spec_helper'
require 'distributed_press/v1/token'

RSpec.describe DistributedPress::V1::Token do
  before do
    payload = build(:token_payload).to_h

    @ecdsa_key = OpenSSL::PKey::EC.generate('prime256v1')
    @token = JWT.encode payload, @ecdsa_key, 'ES256'
  end

  describe '#new' do
    it "can't be initialized without a token" do
      expect do
        DistributedPress::V1::Token.new
      end.to(raise_error(ArgumentError))
    end

    it "can't be initialized without a jwt" do
      expect do
        DistributedPress::V1::Token.new(token: SecureRandom.hex)
      end.to(raise_error(JWT::DecodeError))
    end

    it 'can be initialized with a token' do
      expect(t = DistributedPress::V1::Token.new(token: @token))
      expect(t.header.success?).to(eql(true))
      expect(t.payload.success?).to(eql(true))
    end

    it "can't be initialized with a token with other payload" do
      token = JWT.encode({ random: 'stuff' }, @ecdsa_key, 'ES256')

      expect do
        DistributedPress::V1::Token.new(token: token)
      end.to(raise_error(DistributedPress::V1::TokenPayloadNotValidError))
    end
  end

  context 'Token initialized' do
    before do
      @token = DistributedPress::V1::Token.new(token: @token)
    end

    describe '#expired?' do
      it 'is false for expiration -1' do
        expect(@token.payload[:expires]).to(eql(-1))
        expect(@token.expired?).to(eql(false))
      end
    end

    describe '#publisher?' do
      it 'is true for admin token' do
        expect(@token.payload[:capabilities].include?('publisher')).to(eql(true))
        expect(@token.publisher?).to(eql(true))
      end
    end

    describe '#admin?' do
      it 'is true for admin token' do
        expect(@token.payload[:capabilities].include?('admin')).to(eql(true))
        expect(@token.admin?).to(eql(true))
      end
    end

    describe '#refresh?' do
      it 'is true for admin token' do
        expect(@token.payload[:capabilities].include?('refresh')).to(eql(true))
        expect(@token.refresh?).to(eql(true))
      end
    end
  end
end
